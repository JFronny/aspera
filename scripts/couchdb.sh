#!/bin/sh
# execute couchdb without relying on systemd
# http://localhost:5984/_utils
sudo chpst -u couchdb:daemon /usr/lib/couchdb/bin/couchdb -couch_ini /usr/lib/couchdb/etc/default.ini /usr/lib/couchdb/etc/datadirs.ini /etc/couchdb/local.ini