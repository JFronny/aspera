#!/bin/sh
systemctl stop aspera
su aspera -c "curl -L \"https://gitlab.com/JFronny/aspera/-/jobs/artifacts/master/raw/aspera-ci.jar?job=build_test\" --output /mnt/srv/aspera/aspera.jar"
systemctl start aspera
