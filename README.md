# Aspera
Aspera is my attempt at rewriting some parts of Astra since D#+ is now deprecated and I want to take advantage of newer features (like slash commands).

## Adding to a server
Aspera is not public for now, but when it is, you can add it with [this](https://discordapp.com/oauth2/authorize?client_id=880028480757190667&scope=bot+applications.commands&permissions=3222592) link

## Deploying your own
For testing, you can download the latest jar [here](https://gitlab.com/JFronny/aspera/-/jobs/artifacts/master/raw/aspera-ci.jar?job=build_test).\
However, if you want to stay up-to-date with the latest changes, I recommend you use the scripts provided in scripts/ (they are what I use for the official instance).

## A note on CouchDB
If you are using a raspberry pi with AlARM, you will need to build CouchDB yourself since the version in the repos
no longer works. In order to build it, you will need to replace the spidermonkey package with js185 and set --spidermonkey-version to 1.8.5.
You may also need to disable PGP verification.
I recommend using a system that has a version of CouchDB that works.