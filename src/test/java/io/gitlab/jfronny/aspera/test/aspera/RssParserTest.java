package io.gitlab.jfronny.aspera.test.aspera;

import io.gitlab.jfronny.aspera.test.BaseTest;
import io.gitlab.jfronny.aspera.util.rss.RssInfo;
import io.gitlab.jfronny.aspera.util.rss.RssParser;
import org.junit.jupiter.api.Test;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class RssParserTest extends BaseTest {
    @Test
    void testParse() throws IOException, ParserConfigurationException, SAXException {
        try (BufferedReader br = Files.newBufferedReader(Path.of("test.rss"))) {
            RssInfo info = RssParser.parse(br.lines().collect(Collectors.joining("\n")));
            assertEquals("DER SPIEGEL - International", info.title);
            assertEquals("https://www.spiegel.de/", info.link);
            assertEquals("Deutschlands führende Nachrichtenseite. Alles Wichtige aus Politik, Wirtschaft, Sport, Kultur, Wissenschaft, Technik und mehr.", info.description);
            assertEquals(20, info.items.size());
            for (RssInfo.Item item : info.items) {
                assertEquals("Lagos in Nigeria: A Week in the World’s Most Chaotic City", item.title);
                assertEquals("https://www.spiegel.de/international/world/lagos-in-nigeria-a-week-in-the-world-s-most-chaotic-city-a-6d56ae7e-596b-40f1-b1ad-43ba0778a6c7#ref=rss", item.link);
                assertEquals("Lagos is poised to become the world’s biggest city. The Nigerian megacity is a massive experiment – unregulated and wild, with endless traffic jams, waterfront slums and an impressively resilient population.", item.description);
                assertEquals("https://www.spiegel.de/international/world/lagos-in-nigeria-a-week-in-the-world-s-most-chaotic-city-a-6d56ae7e-596b-40f1-b1ad-43ba0778a6c7", item.guid);
                break;
            }
        }
    }
}
