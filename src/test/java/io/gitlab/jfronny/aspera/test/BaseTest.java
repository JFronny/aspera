package io.gitlab.jfronny.aspera.test;

import io.gitlab.jfronny.aspera.Main;
import io.gitlab.jfronny.aspera.database.data.AsperaConfig;
import org.junit.jupiter.api.BeforeAll;

public class BaseTest {
    @BeforeAll
    static void setup() {
        Main.setConfig(new AsperaConfig());
    }
}
