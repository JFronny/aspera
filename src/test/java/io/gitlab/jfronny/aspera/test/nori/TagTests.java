/*
 * This file is part of nori.
 * Copyright (c) 2014 Tomasz Jan Góralczyk <tomg@fastmail.uk>
 * License: ISC
 */

package io.gitlab.jfronny.aspera.test.nori;

import io.gitlab.jfronny.aspera.test.BaseTest;
import io.gitlab.jfronny.aspera.util.scrape.nori.Tag;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

/**
 * Tests for the {@link Tag} class.
 */
public class TagTests extends BaseTest {
    /**
     * Tests the {@link Tag#equals(Object)}  method.
     */
    @Test
    public void testEquals() {
        final Tag tag1 = new Tag("duck", Tag.Type.CHARACTER);
        final Tag tag2 = new Tag("duck", Tag.Type.CHARACTER);
        final Tag tag3 = new Tag("bird", Tag.Type.CHARACTER);
        final Tag tag4 = new Tag("duck", Tag.Type.ARTIST);

        // Two tags with the same name and type should be equal to one another.
        assertEquals(tag1, tag2);
        assertNotEquals(tag1, tag3);
        assertNotEquals(tag1, tag4);
    }

    /**
     * Tests sorting collections of Tags using the {@link Tag#compareTo(Tag)} method.
     */
    @Test
    public void testCompareTo() {
        final List<Tag> tagList = new ArrayList<>();
        tagList.add(new Tag("duck", Tag.Type.CHARACTER));
        tagList.add(new Tag("quack", Tag.Type.GENERAL));
        tagList.add(new Tag("bird", Tag.Type.ARTIST));

        // Sort the list.
        Collections.sort(tagList);
        // Make sure tags are sorted alphabetically.
        assertEquals(tagList.get(0).getName(), "bird");
        assertEquals(tagList.get(1).getName(), "duck");
        assertEquals(tagList.get(2).getName(), "quack");
    }

    /**
     * Test creating space-separated queries using the {@link Tag#stringFromArray(Tag[])} method.
     */
    @Test
    public void testStringFromArray() {
        final String tags = Tag.stringFromArray(new Tag[]{new Tag("duck"), new Tag("quack")});
        assertEquals(tags, "duck quack");
    }

    /**
     * Test creating tag arrays from space-separated queries using the {@link Tag#arrayFromString(String) method.
     */
    @Test
    public void testArrayFromString() {
        final Tag[] tags = Tag.arrayFromString("duck quack", Tag.Type.CHARACTER);
        assertEquals(2, tags.length);
        assertEquals(tags[0], new Tag("duck", Tag.Type.CHARACTER));
        assertEquals(tags[1], new Tag("quack", Tag.Type.CHARACTER));
    }
}
