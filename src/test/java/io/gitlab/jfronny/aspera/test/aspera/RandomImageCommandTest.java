package io.gitlab.jfronny.aspera.test.aspera;

import io.gitlab.jfronny.aspera.database.data.AsperaConfig;
import io.gitlab.jfronny.aspera.util.scrape.nori.ServiceTypeResolver;
import io.gitlab.jfronny.aspera.util.scrape.nori.clients.SearchClient;
import io.gitlab.jfronny.aspera.test.BaseTest;
import org.junit.jupiter.api.Test;

import java.net.URI;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

public class RandomImageCommandTest extends BaseTest {
    @Test
    public void testBooruDiscovery() {
        for (Map.Entry<String, String> entry : new AsperaConfig().boorus.entrySet()) {
            SearchClient sc = assertDoesNotThrow(() -> ServiceTypeResolver.discoverClient(URI.create(entry.getValue()))).client();
            assertDoesNotThrow(() -> sc.search(sc.getDefaultQuery()),
                    "Could not get image from: " + entry.getKey());
        }
    }
}
