/*
 * This file is part of nori.
 * Copyright (c) 2014-2016 Tomasz Jan Góralczyk <tomg@fastmail.uk>
 * License: GNU GPLv2
 */

package io.gitlab.jfronny.aspera.test.nori;

import io.gitlab.jfronny.aspera.util.scrape.nori.clients.Flickr;
import io.gitlab.jfronny.aspera.util.scrape.nori.clients.SearchClient;

/**
 * Tests for the Flickr SearchClient.
 */
public class FlickrTest extends SearchClientTestCase {
    @Override
    protected SearchClient createSearchClient() {
        return new Flickr(Flickr.FLICKR_API_ENDPOINT.toString());
    }

    /**
     * @return Tag to search for while testing the support of this API.
     */
    @Override
    protected String getDefaultTag() {
        return "duck";
    }
}
