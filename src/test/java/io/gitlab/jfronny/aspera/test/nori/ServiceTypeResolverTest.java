/*
 * This file is part of nori.
 * Copyright (c) 2014 Tomasz Jan Góralczyk <tomg@fastmail.uk>
 * License: ISC
 */

package io.gitlab.jfronny.aspera.test.nori;

import io.gitlab.jfronny.aspera.test.BaseTest;
import io.gitlab.jfronny.aspera.util.scrape.nori.ServiceTypeResolver;
import io.gitlab.jfronny.aspera.util.scrape.nori.clients.*;
import org.junit.jupiter.api.Test;

import java.net.URI;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Test unit for the {@link ServiceTypeResolver} service.
 */
public class ServiceTypeResolverTest extends BaseTest {
    /**
     * Test detection of the Danbooru 2.x API
     */
    @Test
    public void testDanbooruDetection() throws Throwable {
        String url = Danbooru.detectService("https://danbooru.donmai.us");

        assertNotNull(url);
        assertEquals("https://danbooru.donmai.us", url);
    }

    /**
     * Test detection of the Danbooru 1.x API
     */
    @Test
    public void testDanbooruLegacyDetection() throws Throwable {
        String url = DanbooruLegacy.detectService("https://danbooru.donmai.us");

        assertNotNull(url);
        assertEquals("https://danbooru.donmai.us", url);
    }

    /**
     * Test detection of the Gelbooru API.
     */
    @Test
    public void testGelbooruDetection() throws Throwable {
        String url = Gelbooru.detectService("https://safebooru.org");

        assertNotNull(url);
        assertEquals("https://safebooru.org", url);
    }

    /**
     * Test detection of the Shimmie API.
     */
    @Test
    public void testShimmieDetection() {
        // Disable dollbooru tests, as its down.
        //String url = Shimmie.detectService("http://dollbooru.org");

        //assertNotNull(url);
        //assertEquals("http://dollbooru.org", url);
    }

    /**
     * Test detection of Moebooru (Danbooru 1.x fork) boards.
     */
    @Test
    public void testMoebooruDetection() throws Throwable {
        String url = DanbooruLegacy.detectService("https://yande.re");

        assertNotNull(url);
        assertEquals("https://yande.re", url);
    }

    /**
     * Test detection of the flickr API.
     */
    @Test
    public void testFlickrDetection() {
        String url = Flickr.detectService(URI.create("https://api.flickr.com"));

        assertNotNull(url);
        assertEquals("https://api.flickr.com/services/rest", url);
    }

    /**
     * Test detection of the flickr user API.
     */
    @Test
    public void testFlickrUserDetection() {
        String url = FlickrUser.detectService(URI.create("http://flickr.com/photos/128962151@N05"));

        assertNotNull(url);
        assertEquals("https://flickr.com/photos/128962151@N05", url);
    }

    /**
     * Test detection of sites that do not expose a supported API
     */
    @Test
    public void testNoServiceDetected() {
        assertThrows(Exception.class, () -> ServiceTypeResolver.discoverClient(new URI("http://google.com")));
    }
}
