/*
 * This file is part of nori.
 * Copyright (c) 2014 Tomasz Jan Góralczyk <tomg@fastmail.uk>
 * License: ISC
 */

package io.gitlab.jfronny.aspera.test.nori;


import io.gitlab.jfronny.aspera.test.BaseTest;
import io.gitlab.jfronny.aspera.util.scrape.nori.Image;
import io.gitlab.jfronny.aspera.util.scrape.nori.Tag;
import org.apache.http.util.TextUtils;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.Locale;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Tests and utilities for testing the {@link Image} class.
 */
public class ImageTests extends BaseTest {
    public static final Logger LOGGER = LoggerFactory.getLogger("AsperaTest");
    /**
     * RegEx Pattern used for matching URLs.
     * Courtesy of John Gruber (http://daringfireball.net/2010/07/improved_regex_for_matching_urls)
     * (Public Domain)
     */
    private static final String urlPattern = "(?i)\\b((?:https?://|www\\d{0,3}[.]|[a-z0-9.\\-]+[.][a-z]{2,4}/)(?:[^\\s()<>]+|\\(([^\\s()<>]+|(\\([^\\s()<>]+\\)))*\\))+(?:\\(([^\\s()<>]+|(\\([^\\s()<>]+\\)))*\\)|[^\\s`!()\\[\\]{};:'\".,<>?«»“”‘’]))";
    /**
     * RegEx pattern for matching numerical Strings (used for IDs).
     */
    private static final String integerPattern = "^\\d+$";

    /**
     * Get an Image suitable for testing.
     */
    public static Image getMockImage(Image.SafeSearchRating safeSearchRating, Tag... tags) {
        final Image image = new Image();
        image.fileUrl = "http://awesomeboorusite.org/data/images/image.png";
        image.width = 1000;
        image.height = 900;
        image.previewUrl = "http://awesomeboorusite.org/data/previews/image.png";
        image.previewWidth = 150;
        image.previewHeight = 130;
        image.sampleUrl = "http://awesomeboorusite.org/data/samples/image.png";
        image.sampleWidth = 850;
        image.sampleHeight = 800;
        image.tags = tags.clone();
        image.id = "123456";
        image.parentId = "123455";
        image.webUrl = "http://awesomeboorusite.org/post/view/image";
        image.pixivId = "111222333";
        image.safeSearchRating = safeSearchRating;
        image.score = 23;
        image.source = "http://pixiv.com/duck.png";
        image.md5 = "cfaf278e8f522c72644cee2a753d2845";
        image.searchPage = 0;
        image.searchPagePosition = 1;
        image.createdAt = new Date(1398902400);

        return image;
    }

    /**
     * Verify validity of an {@link Image} object.
     * Used to ensure that Image values returned by each individual API client are correct.
     */
    public static void verifyImage(Image image) {
        // Verify URLs.
        assertTrue(image.fileUrl.matches(urlPattern));
        assertTrue(image.previewUrl.matches(urlPattern));
        assertTrue(image.sampleUrl.matches(urlPattern));
        assertTrue(image.webUrl.matches(urlPattern));

        // Verify image sizes are set and that they are positive integers.
        assertTrue(image.width > 0);
        assertTrue(image.height > 0);
        if (image.previewWidth == 0)
            LOGGER.info(String.format(Locale.US, "Preview width was 0 for image: %s", image.webUrl));
        assertTrue(image.previewWidth >= 0);
        if (image.previewHeight == 0)
            LOGGER.info(String.format(Locale.US, "Preview height was 0 for image: %s", image.webUrl));
        assertTrue(image.previewHeight >= 0);
        if (image.sampleWidth == 0)
            LOGGER.info(String.format(Locale.US, "Sample width was 0 for image: %s", image.webUrl));
        assertTrue(image.sampleWidth >= 0);
        if (image.sampleHeight == 0)
            LOGGER.info(String.format(Locale.US, "Sample height was 0 for image: %s", image.webUrl));
        assertTrue(image.sampleHeight >= 0);

        // Verify tags.
        if (image.tags.length == 0)
            LOGGER.info(String.format(Locale.US, "No tags for image: %s", image.webUrl));
        for (Tag tag : image.tags) {
            assertFalse(TextUtils.isEmpty(tag.getName()));
            assertNotNull(tag.getType());
        }

        // Verify numerical strings (IDs)
        assertFalse(TextUtils.isEmpty(image.id));
        assertTrue(image.id.matches(integerPattern));
        if (image.parentId != null && !image.parentId.isEmpty())
            assertTrue(image.parentId.matches(integerPattern));
        else
            LOGGER.info(String.format(Locale.US, "No parent ID for image: %s", image.webUrl));
        if (image.pixivId != null) {
            assertFalse(TextUtils.isEmpty(image.pixivId));
            assertTrue(image.pixivId.matches(integerPattern));
        } else
            LOGGER.info(String.format(Locale.US, "No Pixiv ID for image: %s", image.webUrl));

        // Misc stuff.
        assertNotNull(image.safeSearchRating);
        if (image.source == null || image.source.isEmpty())
            LOGGER.info(String.format(Locale.US, "No source for image: %s", image.webUrl));
        else
            assertFalse(TextUtils.isEmpty(image.source));
        assertEquals(image.md5.length(), 32); // MD5 hashes are always 32 characters long.
        assertTrue(image.searchPage >= 0);
        assertTrue(image.searchPagePosition >= 0);
        assertNotNull(image.createdAt);
    }

    /**
     * Tests the {@link Image#getPixivIdFromUrl(String)} method.
     */
    @Test
    public void testGetPixivIdFromUrl() {
        assertEquals(Image.getPixivIdFromUrl("http://www.pixiv.net/member_illust.php?mode=medium&illust_id=44466677"), "44466677");
    }

    @Test
    public void testGetFileExtension() {
        Image image = getMockImage(Image.SafeSearchRating.S, new Tag("duck"), new Tag("bird"));

        assertEquals(image.getFileExtension(), "png");
        image.fileUrl = "http://awesomeboorusite.org/data/images/image.jpg";
        assertEquals(image.getFileExtension(), "jpg");
        image.fileUrl = "http://awesomeboorusite.org/data/images/image.jpeg";
        assertEquals(image.getFileExtension(), "jpg");
        image.fileUrl = "http://awesomeboorusite.org/data/images/image.gif";
        assertEquals(image.getFileExtension(), "gif");
        image.fileUrl = "http://awesomeboorusite.org/data/images/image.webm";
        assertEquals(image.getFileExtension(), "webm");
        image.fileUrl = "http://awesomeboorusite.org/data/images/image";
        assertNull(image.getFileExtension());
        image.fileUrl = "http://awesomeboorusite.org";
        assertNull(image.getFileExtension());
    }

    /**
     * Tests the {@link Image.SafeSearchRating#fromString(String)} method.
     */
    @Test
    public void testSafeSearchRatingFromString() {
        assertEquals(Image.SafeSearchRating.fromString("Safe"), Image.SafeSearchRating.S);
        assertEquals(Image.SafeSearchRating.fromString("Questionable"), Image.SafeSearchRating.Q);
        assertEquals(Image.SafeSearchRating.fromString("Explicit"), Image.SafeSearchRating.E);
        assertEquals(Image.SafeSearchRating.fromString("Undefined"), Image.SafeSearchRating.U);
    }
}
