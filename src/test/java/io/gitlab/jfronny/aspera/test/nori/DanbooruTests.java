/*
 * This file is part of nori.
 * Copyright (c) 2014 Tomasz Jan Góralczyk <tomg@fastmail.uk>
 * License: ISC
 */

package io.gitlab.jfronny.aspera.test.nori;

import io.gitlab.jfronny.aspera.util.scrape.nori.clients.Danbooru;
import io.gitlab.jfronny.aspera.util.scrape.nori.clients.SearchClient;

/**
 * Tests for the Danbooru 2.x API.
 */
public class DanbooruTests extends SearchClientTestCase {
    @Override
    protected SearchClient createSearchClient() {
        return new Danbooru("https://danbooru.donmai.us");
    }

    @Override
    protected String getDefaultTag() {
        return "blonde_hair";
    }
}
