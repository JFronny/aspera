/*
 * This file is part of nori.
 * Copyright (c) 2014 Tomasz Jan Góralczyk <tomg@fastmail.uk>
 * License: ISC
 */

package io.gitlab.jfronny.aspera.test.nori;

/**
 * Tests for the Shimmie2 API client.
 */
public class ShimmieTests /*extends SearchClientTestCase*/ {
    /** DISABLE DOLLBOORU TESTS TEMPORARILY
     @Override protected SearchClient createSearchClient() {
     return new Shimmie(getInstrumentation().getContext(), "Dollbooru", "http://dollbooru.org");
     }

     @Override protected String getDefaultTag() {
     return "blonde_hair";
     }
     */
}
