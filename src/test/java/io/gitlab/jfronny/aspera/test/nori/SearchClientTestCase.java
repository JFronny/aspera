/*
 * This file is part of nori.
 * Copyright (c) 2014 Tomasz Jan Góralczyk <tomg@fastmail.uk>
 * License: ISC
 */

package io.gitlab.jfronny.aspera.test.nori;

import io.gitlab.jfronny.aspera.test.BaseTest;
import io.gitlab.jfronny.aspera.util.scrape.nori.Image;
import io.gitlab.jfronny.aspera.util.scrape.nori.SearchResult;
import io.gitlab.jfronny.aspera.util.scrape.nori.clients.SearchClient;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Extend this class to test a class implementing the {@link SearchClient} API.
 */
public abstract class SearchClientTestCase extends BaseTest {
    @Test
    public void testSearchUsingTags() throws Throwable {
        // Create a new client connected to the Danbooru API.
        final SearchClient client = createSearchClient();
        // Retrieve a search result.
        final SearchResult result = client.search(getDefaultTag());

        // Make sure we got results back.
        assertNotEquals(0, result.getImages().length);
        // Verify metadata for each returned image.
        for (Image image : result.getImages()) {
            ImageTests.verifyImage(image);
        }

        // Check rests of the values.
        assertEquals(result.getCurrentOffset(), 0);
        assertEquals(1, result.getQuery().length);
        assertEquals(result.getQuery()[0].getName(), getDefaultTag());
        assertTrue(result.hasNextPage());
    }

    @Test
    public void testSearchUsingTagsAndOffset() throws Throwable {
        // Create a new client connected to the Danbooru API.
        final SearchClient client = createSearchClient();
        // Retrieve search results.
        final SearchResult page1 = client.search(getDefaultTag(), 0);
        final SearchResult page2 = client.search(getDefaultTag(), 1);

        // Make sure that the results differ.
        assertNotEquals(0, page1.getImages().length);
        assertNotEquals(0, page2.getImages().length);
        assertEquals(1, page2.getCurrentOffset());
        assertNotEquals(page2.getImages()[0].id, page1.getImages()[0].id);
        assertEquals(page1.getCurrentOffset(), page1.getImages()[0].searchPage);
        assertEquals(0, page1.getImages()[0].searchPagePosition);
        assertEquals(1, page1.getImages()[1].searchPagePosition);
        assertEquals(page2.getCurrentOffset(), page2.getImages()[0].searchPage);
        assertEquals(0, page2.getImages()[0].searchPagePosition);
        assertEquals(1, page2.getImages()[1].searchPagePosition);
    }

    @Test
    public void testGetDefaultQuery() {
        final SearchClient client = createSearchClient();
        assertNotNull(client.getDefaultQuery());
    }

    @Test
    public void testRequiredAuthentication() {
        final SearchClient client = createSearchClient();
        assertNotNull(client.getDefaultQuery());
    }

    protected abstract SearchClient createSearchClient();

    /**
     * @return Tag to search for while testing the support of this API.
     */
    protected abstract String getDefaultTag();

}
