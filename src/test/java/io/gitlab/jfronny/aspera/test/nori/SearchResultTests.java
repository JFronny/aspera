/*
 * This file is part of nori.
 * Copyright (c) 2014 Tomasz Jan Góralczyk <tomg@fastmail.uk>
 * License: ISC
 */

package io.gitlab.jfronny.aspera.test.nori;

import io.gitlab.jfronny.aspera.test.BaseTest;
import io.gitlab.jfronny.aspera.util.scrape.nori.Image;
import io.gitlab.jfronny.aspera.util.scrape.nori.SearchResult;
import io.gitlab.jfronny.aspera.util.scrape.nori.Tag;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Tests the {@link SearchResult} test.
 */
public class SearchResultTests extends BaseTest {
    /**
     * Create a SearchResult with fake data suitable for testing.
     */
    public static SearchResult getMockSearchResult() {
        final Image[] images = new Image[]{
                ImageTests.getMockImage(Image.SafeSearchRating.S,
                        new Tag("duck"), new Tag("quack")),
                ImageTests.getMockImage(Image.SafeSearchRating.Q,
                        new Tag("bird"))
        };
        return new SearchResult(images, new Tag[]{new Tag("Tag")}, 0);
    }

    /**
     * Tests the {@link SearchResult#filter(Tag...)}  method.
     */
    @Test
    public void testFilterWithTags() {
        final SearchResult searchResult = getMockSearchResult();
        searchResult.filter(new Tag("duck"));
        assertEquals(1, searchResult.getImages().length);
        assertEquals(searchResult.getImages()[0].tags[0].getName(), "bird");
        assertEquals(searchResult.getImages()[0].searchPagePosition, 0);
    }

    /**
     * Tests the {@link SearchResult#addImages(Image[], int)} method.
     */
    @Test
    public void testAddImages() {
        final SearchResult searchResult = getMockSearchResult();
        searchResult.addImages(new Image[]{ImageTests.getMockImage(Image.SafeSearchRating.E, new Tag("quack"))}, 20);
        assertEquals(3, searchResult.getImages().length);
        assertEquals(searchResult.getImages()[2].safeSearchRating, Image.SafeSearchRating.E);
        assertEquals(searchResult.getCurrentOffset(), 20);
    }

    /**
     * Tests the {@link SearchResult#getImages()} method.
     */
    @Test
    public void testGetImages() {
        final SearchResult searchResult = getMockSearchResult();
        assertNotEquals(0, searchResult.getImages().length);
    }

    /**
     * Tests the {@link SearchResult#filter(Image.SafeSearchRating...)} method.
     */
    @Test
    public void testFilterWithSafeSearchRating() {
        final SearchResult searchResult = getMockSearchResult();
        searchResult.filter(Image.SafeSearchRating.Q);
        assertEquals(1, searchResult.getImages().length);
        assertEquals(searchResult.getImages()[0].safeSearchRating, Image.SafeSearchRating.Q);
        assertEquals(searchResult.getImages()[0].searchPagePosition, 0);
    }

    /**
     * Tests the {@link SearchResult#getCurrentOffset()} method.
     */
    @Test
    public void testGetCurrentOffset() {
        final SearchResult searchResult = getMockSearchResult();
        assertEquals(searchResult.getCurrentOffset(), 0);
        searchResult.addImages(new Image[]{searchResult.getImages()[0]}, 30);
        assertEquals(searchResult.getCurrentOffset(), 30);
    }

    /**
     * Tests the {@link SearchResult#onLastPage()} method.
     */
    @Test
    public void testOnLastPage() {
        final SearchResult searchResult = getMockSearchResult();
        assertTrue(searchResult.hasNextPage());
        searchResult.onLastPage();
        assertFalse(searchResult.hasNextPage());
    }

    /**
     * Tests the {@link SearchResult#getSearchResultForPage(int) method.}
     */
    @Test
    public void testGetSearchResultForPage() {
        final SearchResult searchResult = getMockSearchResult();
        Image image = ImageTests.getMockImage(Image.SafeSearchRating.E, new Tag("quack"));
        image.searchPage = 1;
        image.searchPagePosition = 0;
        searchResult.addImages(new Image[]{image}, 1);
        SearchResult filteredSearchResult = searchResult.getSearchResultForPage(1);
        assertEquals(0, searchResult.getImages()[0].searchPage);
        assertEquals(1, filteredSearchResult.getImages()[0].searchPage);
    }
}
