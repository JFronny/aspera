/*
 * This file is part of nori.
 * Copyright (c) 2014 Tomasz Jan Góralczyk <tomg@fastmail.uk>
 * License: ISC
 */

package io.gitlab.jfronny.aspera.test.nori;

import io.gitlab.jfronny.aspera.util.scrape.nori.clients.DanbooruLegacy;
import io.gitlab.jfronny.aspera.util.scrape.nori.clients.SearchClient;

/**
 * Tests support for Moebooru-based boards in the DanbooruLegacy client.
 */
public class MoebooruTest extends SearchClientTestCase {
    @Override
    protected SearchClient createSearchClient() {
        return new DanbooruLegacy("https://yande.re");
    }

    @Override
    protected String getDefaultTag() {
        return "hatsune_miku";
    }
}
