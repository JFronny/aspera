/*
 * This file is part of nori.
 * Copyright (c) 2014 Tomasz Jan Góralczyk <tomg@fastmail.uk>
 * License: ISC
 */

package io.gitlab.jfronny.aspera.test.nori;

import io.gitlab.jfronny.aspera.util.scrape.nori.clients.Gelbooru;
import io.gitlab.jfronny.aspera.util.scrape.nori.clients.SearchClient;

/**
 * Tests for the Gelbooru API client.
 */
public class GelbooruTest extends SearchClientTestCase {
    @Override
    protected SearchClient createSearchClient() {
        return new Gelbooru("https://gelbooru.com");
    }

    @Override
    protected String getDefaultTag() {
        return "blonde_hair";
    }
}
