package io.gitlab.jfronny.aspera.commands;

import io.gitlab.jfronny.aspera.Main;
import io.gitlab.jfronny.aspera.util.commands.Command;
import io.gitlab.jfronny.aspera.util.commands.CommandGroup;
import io.gitlab.jfronny.aspera.util.commands.CommandTag;
import io.gitlab.jfronny.aspera.util.commands.Parameter;
import io.gitlab.jfronny.aspera.util.commands.slash.DiscordParameter;
import net.dv8tion.jda.api.Permission;

public class HelpCommand extends Command {
    @Parameter(description = "The command to get help for", required = false)
    public String command;

    @Parameter(description = "The subcommand. May also be included in the command", required = false)
    public String subcommand;

    public HelpCommand() throws Exception {
        super("help", "Allows you to view the docs also available in the slash commands overview");
    }

    @Override
    public void execute() throws Exception {
        StringBuilder help = new StringBuilder("```");
        if (command != null) {
            if (subcommand != null) command += " " + subcommand;
            getHelp(Main.getBot().getBackend().getCommand(command, guild), help, DetailLevel.Full, "");
        }
        else {
            for (Command command : Main.getBot().getCommands()) {
                getHelp(command, help, DetailLevel.List, "");
            }
        }
        help.append("\n```");
        reply(help.toString());
    }

    private void getHelp(Command hCmd, StringBuilder help, DetailLevel detailLevel, String linePrefix) {
        if (hCmd.hasTag(CommandTag.AuthorOnly) && !user.getId().equals(Main.getConfig().botOwner))
            return;
        if (hCmd.hasTag(CommandTag.GuildOnly) && !args.isGuild())
            return;
        if (hCmd.hasTag(CommandTag.AdminOnly) && args.isGuild() && args.callerHasPermission(Permission.ADMINISTRATOR))
            return;
        if (hCmd.hasTag(CommandTag.NsfwOnly) && !args.isNsfwAllowed())
            return;
        switch (detailLevel) {
            case Full -> {
                help.append('\n').append(linePrefix).append(hCmd.getName()).append(": ").append(hCmd.getDescription());
                if (!hCmd.getOptions().isEmpty()) {
                    help.append('\n').append(linePrefix).append("Options:");
                    for (DiscordParameter option : hCmd.getOptions()) {
                        help.append('\n').append(linePrefix).append("- ").append(option.getName());
                        if (!option.isRequired()) help.append(" (optional)");
                        help.append(": ").append(option.getDescription());
                    }
                }
                if (hCmd instanceof CommandGroup cg) {
                    help.append('\n').append(linePrefix).append("Commands:");
                    for (Command cmd : cg.getSubcommands().values()) {
                        getHelp(cmd, help, DetailLevel.List, "- ");
                    }
                }
            }
            case List -> {
                help.append('\n').append(linePrefix).append(hCmd.getName()).append(": ").append(hCmd.getDescription());
                if (hCmd instanceof CommandGroup cg) {
                    help.append(" - commands:");
                    for (Command cmd : cg.getSubcommands().values()) {
                        getHelp(cmd, help, DetailLevel.Sub, "");
                    }
                }
            }
            case Sub -> help.append(' ').append(hCmd.getName());
        }
    }

    enum DetailLevel {
        Full,
        List,
        Sub
    }
}
