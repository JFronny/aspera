package io.gitlab.jfronny.aspera.commands.admin;

import io.gitlab.jfronny.aspera.Main;
import io.gitlab.jfronny.aspera.database.data.GuildData;
import io.gitlab.jfronny.aspera.util.commands.Command;
import io.gitlab.jfronny.aspera.util.commands.CommandTag;
import io.gitlab.jfronny.aspera.util.commands.Parameter;
import io.gitlab.jfronny.aspera.util.rss.RssParser;

import java.io.IOException;

public class AddRssSource extends Command {
    @Parameter(description = "The URL of the RSS source", required = true)
    public String url;

    public AddRssSource() throws Exception {
        super("add-rss-source", "Adds an RSS source to the current channel", CommandTag.AdminOnly, CommandTag.GuildOnly);
    }

    @Override
    public void execute() throws IOException {
        String sourceTitle;
        try {
            sourceTitle = RssParser.fetch(url).title;
        }
        catch (Throwable t) {
            reply("Could not fetch source, not adding");
            return;
        }
        GuildData gd = Main.getDatabase().getGuild(guild);
        gd.getRssConfigs().putIfAbsent(channel.getId(), new GuildData.RssConfig());
        gd.getRssConfigs().get(channel.getId()).getSources().add(url);
        Main.getDatabase().setGuild(gd);
        reply("Added " + sourceTitle);
    }
}
