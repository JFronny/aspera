package io.gitlab.jfronny.aspera.commands;

import io.gitlab.jfronny.aspera.Main;
import io.gitlab.jfronny.aspera.util.commands.Command;
import io.gitlab.jfronny.aspera.util.commands.Parameter;
import net.dv8tion.jda.api.entities.TextChannel;

import java.io.IOException;

public class PostCommand extends Command {
    @Parameter(description = "The anon channel to post in", required = true)
    public TextChannel channel;

    @Parameter(description = "The message to post", required = true)
    public String message;

    public PostCommand() throws Exception {
        super("post", "Posts a message to an anon channel");
    }

    @Override
    public void execute() {
        if (channel == null) {
            reply("That is not a known channel");
            return;
        }
        try {
            if (!Main.getDatabase().getGuild(channel.getGuild()).getAnonChannels().contains(channel.getId())) {
                reply("That (" + channel.getId() + ") is not an anon channel");
                return;
            }
        } catch (IOException e) {
            Main.LOGGER.error("Could not check whether channel is anon channel", e);
            reply("Something went wrong on our end, please contact the bots administrator");
            return;
        }
        if (!channel.getGuild().isMember(user)) {
            reply("You aren't even a member of that server!");
            return;
        }
        channel.sendMessage(message).queue();
        reply("Done");
    }
}
