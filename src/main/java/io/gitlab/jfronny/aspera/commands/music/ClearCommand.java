package io.gitlab.jfronny.aspera.commands.music;

import io.gitlab.jfronny.aspera.util.commands.Command;
import io.gitlab.jfronny.aspera.util.commands.CommandTag;

public class ClearCommand extends Command {
    public ClearCommand() throws Exception {
        super("clear", "Clears the queue", CommandTag.MusicOnly);
    }

    @Override
    public void execute() {
        if (!music.isConnected() || music.tracks.isEmpty()) {
            reply("Not playing anything.");
            return;
        }
        music.tracks.clear();
        music.refreshUI();
        reply("Cleared!");
    }
}
