package io.gitlab.jfronny.aspera.commands.board;

import io.gitlab.jfronny.aspera.util.HttpUtils;
import io.gitlab.jfronny.aspera.util.commands.Command;
import io.gitlab.jfronny.aspera.util.commands.Parameter;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.MessageBuilder;

public class InspirobotCommand extends Command {
    @Parameter(description = "Whether to send christmas quotes", required = false)
    public Boolean xmas;

    public InspirobotCommand() throws Exception {
        super("inspirobot", "Sends a random inspirobot quote");
    }

    @Override
    public void execute() {
        String s = HttpUtils.get("https://inspirobot.me/api?generate=true" + ((xmas != null && xmas) ? "&season=xmas" : "")).sendString();
        reply(new MessageBuilder().setEmbeds(new EmbedBuilder().setImage(s).build()).build());
    }
}
