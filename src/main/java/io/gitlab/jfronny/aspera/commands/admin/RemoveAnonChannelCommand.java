package io.gitlab.jfronny.aspera.commands.admin;

import io.gitlab.jfronny.aspera.Main;
import io.gitlab.jfronny.aspera.database.data.GuildData;
import io.gitlab.jfronny.aspera.util.commands.Command;
import io.gitlab.jfronny.aspera.util.commands.CommandTag;
import io.gitlab.jfronny.aspera.util.commands.Parameter;
import io.gitlab.jfronny.aspera.util.music.MusicChannelMessageHandler;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.MessageChannel;

import java.io.IOException;

public class RemoveAnonChannelCommand extends Command {
    @Parameter(description = "The channel to turn into an anon channel", required = true)
    public MessageChannel channel;

    public RemoveAnonChannelCommand() throws Exception {
        super("remove-anon-channel", "Makes a channel no longer anon", CommandTag.AdminOnly, CommandTag.GuildOnly);
    }

    @Override
    public void execute() {
        try {
            if (!args.callerHasPermission(Permission.ADMINISTRATOR)) {
                reply("Only administrators can perform this action!");
                return;
            }
            GuildData data = Main.getDatabase().getGuild(guild);
            if (!data.getAnonChannels().contains(channel.getId())) {
                reply("This channel was already not an anon channel");
                return;
            }
            data.getAnonChannels().remove(channel.getId());
            Main.getDatabase().setGuild(data);
            MusicChannelMessageHandler.refreshUI(guild);
            reply("Done!");
        } catch (IOException e) {
            reply("Could not set channel");
            Main.LOGGER.error("Could not set music channel", e);
        }
    }
}
