package io.gitlab.jfronny.aspera.commands.music;

import io.gitlab.jfronny.aspera.util.commands.Command;
import io.gitlab.jfronny.aspera.util.commands.CommandTag;
import io.gitlab.jfronny.aspera.util.commands.Parameter;
import io.gitlab.jfronny.aspera.util.music.TrackWrapper;
import net.dv8tion.jda.api.entities.GuildVoiceState;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URIBuilder;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;

public class MixCommand extends Command {
    @Parameter(description = "The query", required = true)
    public String query;

    public MixCommand() throws Exception {
        super("mix", "Plays a random selection of relevant pieces of music based on the query", CommandTag.MusicOnly);
    }

    @Override
    public void execute() {
        if (!music.isConnected()) {
            GuildVoiceState state = args.getUserVoiceState();
            if (state != null && state.inVoiceChannel()) {
                music.connect(state.getChannel());
            }
            else {
                reply("Hey, you can only use this in a voice channel");
                return;
            }
        }
        if (query == null) {
            reply("You must provide a query as the starting point of the mix");
            return;
        }
        AtomicBoolean listMix = new AtomicBoolean(false);
        if (query.startsWith("http://") || query.startsWith("https://")) {
            if (expandUriQuery(query, "list", s -> Set.of(s.setValue("RDAMPL" + s.getValue())),mix -> {
                query = mix;
                listMix.set(true);
            }) == Status.Error)
                return;
        }
        music.search(query, user, (songs, name) -> {
            int count = songs.size();
            switch (count) {
                case 0 -> reply("No matches found");
                case 1 -> {
                    if (expandUriQuery(songs.get(0).getInfo().uri, "v", s -> Set.of(s.setValue("RDAMVM" + s.getValue()).setName("list"), s), mix -> {
                        music.search(mix, user, (mixSongs, mixName) -> {
                            reply("Queuing " + mixSongs.size() + " items from mix");
                            mixSongs.remove(0);
                            music.queue(songs.get(0), this);
                            for (TrackWrapper song : mixSongs) {
                                music.queue(song, this);
                            }
                        }, this);
                    }) != Status.Success) {
                        reply("Something went wrong");
                    }
                }
                default -> {
                    if (listMix.get()) {
                        reply("Queuing " + count + " items from " + name);
                        for (TrackWrapper song : songs) {
                            music.queue(song, this);
                        }
                    }
                    else reply("Not a valid youtube playlist");
                }
            }
        }, this);
    }

    private Status expandUriQuery(String query, String paramName, Transformer<MutableNameValuePair> transformer, Consumer<String> success) {
        try {
            URIBuilder builder = new URIBuilder(query);
            if (builder.getQueryParams().stream().anyMatch(s -> s.getName().equals(paramName))) {
                List<NameValuePair> entries = new ArrayList<>();
                for (NameValuePair entry : builder.getQueryParams()) {
                    if (entry.getName().equals(paramName))
                        entries.addAll(transformer.transform(new MutableNameValuePair(entry)));
                    else entries.add(entry);
                }
                builder.setParameters(entries);
                query = builder.build().toString();
                success.accept(query);
                return Status.Success;
            }
            else
                return Status.NotPresent;
        } catch (URISyntaxException e) {
            e.printStackTrace();
            reply("Something went wrong.");
            return Status.Error;
        }
    }

    enum Status {
        NotPresent, Error, Success
    }

    private static class MutableNameValuePair implements NameValuePair {
        private String name;
        private String value;
        public MutableNameValuePair(NameValuePair pair) {
            name = pair.getName();
            value = pair.getValue();
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public String getValue() {
            return value;
        }

        public MutableNameValuePair setName(String name) {
            MutableNameValuePair mvp = new MutableNameValuePair(this);
            mvp.name = name;
            return mvp;
        }

        public MutableNameValuePair setValue(String value) {
            MutableNameValuePair mvp = new MutableNameValuePair(this);
            mvp.value = value;
            return mvp;
        }
    }

    private interface Transformer<T> {
        Collection<T> transform(T value);
    }
}
