package io.gitlab.jfronny.aspera.commands.debug;

import io.gitlab.jfronny.aspera.Main;
import io.gitlab.jfronny.aspera.util.commands.Command;
import io.gitlab.jfronny.aspera.util.commands.CommandTag;

public class ShutdownCommand extends Command {
    public ShutdownCommand() throws Exception {
        super("shutdown", "Shuts down this bot instance", CommandTag.AuthorOnly);
    }

    @Override
    public void execute() {
        reply("Shutting down!");
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            Main.LOGGER.error("Couldn't wait 500ms to ensure bot shutdown. This is probably fine", e);
        }
        Main.getBot().getApi().shutdown();
    }
}
