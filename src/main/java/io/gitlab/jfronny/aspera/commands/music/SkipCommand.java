package io.gitlab.jfronny.aspera.commands.music;

import io.gitlab.jfronny.aspera.util.commands.Command;
import io.gitlab.jfronny.aspera.util.commands.CommandTag;

public class SkipCommand extends Command {
    public SkipCommand() throws Exception {
        super("skip", "Skips the current song", CommandTag.MusicOnly);
    }

    @Override
    public void execute() {
        if (!music.isConnected()) {
            reply("Not playing anything.");
            return;
        }
        music.playNext(true);
        reply("Skipped.");
    }
}
