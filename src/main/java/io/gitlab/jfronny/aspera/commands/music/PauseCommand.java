package io.gitlab.jfronny.aspera.commands.music;

import io.gitlab.jfronny.aspera.util.commands.Command;
import io.gitlab.jfronny.aspera.util.commands.CommandTag;

public class PauseCommand extends Command {
    public PauseCommand() throws Exception {
        super("pause", "Pauses or resumes the current song", CommandTag.MusicOnly);
    }

    @Override
    public void execute() {
        if (!music.isConnected()) {
            reply("Not playing anything.");
            return;
        }
        music.setPaused(!music.isPaused(), this);
    }
}
