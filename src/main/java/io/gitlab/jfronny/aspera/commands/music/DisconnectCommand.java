package io.gitlab.jfronny.aspera.commands.music;

import io.gitlab.jfronny.aspera.util.commands.Command;
import io.gitlab.jfronny.aspera.util.commands.CommandTag;

public class DisconnectCommand extends Command {
    public DisconnectCommand() throws Exception {
        super("disconnect", "Disconnects the bot if it is in a channel", CommandTag.MusicOnly);
    }

    @Override
    public void execute() {
        if (music.isConnected()) {
            music.disconnect();
            reply("Disconnected!");
        }
        else {
            reply("Not currently in a channel");
        }
    }
}
