package io.gitlab.jfronny.aspera.commands.admin;

import io.gitlab.jfronny.aspera.Main;
import io.gitlab.jfronny.aspera.database.data.GuildData;
import io.gitlab.jfronny.aspera.util.commands.Command;
import io.gitlab.jfronny.aspera.util.commands.CommandTag;
import io.gitlab.jfronny.aspera.util.commands.Parameter;

import java.io.IOException;
import java.util.List;

public class RemoveRssSource extends Command {
    @Parameter(description = "The number of the RSS source (see /admin list-rss-sources)", required = true)
    public Integer source;

    public RemoveRssSource() throws Exception {
        super("remove-rss-source", "Removes an RSS source from the current channel", CommandTag.AdminOnly, CommandTag.GuildOnly);
    }

    @Override
    public void execute() throws IOException {
        GuildData gd = Main.getDatabase().getGuild(guild);
        gd.getRssConfigs().putIfAbsent(channel.getId(), new GuildData.RssConfig());
        List<String> sources = gd.getRssConfigs().get(channel.getId()).getSources();
        if (source > 0 && source <= sources.size()) {
            String source = sources.remove(this.source - 1);
            Main.getDatabase().setGuild(gd);
            reply("Removed " + source);
        }
        else {
            reply("This source does not exist");
        }
    }
}
