package io.gitlab.jfronny.aspera.commands.music;

import io.gitlab.jfronny.aspera.util.commands.Command;
import io.gitlab.jfronny.aspera.util.commands.CommandTag;

import java.util.Collections;

public class ShuffleCommand extends Command {
    public ShuffleCommand() throws Exception {
        super("shuffle", "Shuffles the queue", CommandTag.MusicOnly);
    }

    @Override
    public void execute() {
        Collections.shuffle(music.tracks);
        music.refreshUI();
        reply("Shuffled.");
    }
}
