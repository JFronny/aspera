package io.gitlab.jfronny.aspera.commands.board;

import com.fasterxml.jackson.core.type.TypeReference;
import io.gitlab.jfronny.aspera.Main;
import io.gitlab.jfronny.aspera.util.HttpUtils;
import io.gitlab.jfronny.aspera.util.commands.Command;
import io.gitlab.jfronny.aspera.util.commands.Parameter;
import io.gitlab.jfronny.aspera.util.scrape.reddit.SubredditModel;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.entities.MessageEmbed;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.List;

public class RedditCommand extends Command {
    @Parameter(description = "The subreddit to send an image from or none for a random one", required = false)
    public String subreddit;

    public RedditCommand() throws Exception {
        super("reddit", "Posts a random image from a subreddit");
    }

    @Override
    public void execute() throws IOException {
        InputStream is = HttpUtils.get("https://www.reddit.com/r/" + (subreddit == null ? "random" : subreddit) + "/random/.json").sendInputStream();
        SubredditModel.Data.Child.ChildData data = Main.JACKSON.readValue(is, new TypeReference<List<SubredditModel>>() {}).get(0).data.children.get(0).data;
        if (data.over_18 && !args.isNsfwAllowed()) {
            reply("A post was found but wasn't worksafe");
            return;
        }
        String content = data.author + " on " + data.subreddit_name_prefixed;
        MessageEmbed embed = new EmbedBuilder()
                .setTitle(data.title, "https://www.reddit.com" + data.permalink)
                .setDescription(data.selftext).build();
        try {
            reply(HttpUtils.get(data.url).sendInputStream(), new URL(data.url).getFile())
                    .setContent(content)
                    .setEmbeds(embed)
                    .send();
        }
        catch (Throwable t) {
            try {
                reply(HttpUtils.get(data.media.reddit_video.fallback_url).sendInputStream(), new URL(data.media.reddit_video.fallback_url).getFile())
                        .setContent(content)
                        .setEmbeds(embed)
                        .send();
            }
            catch (Throwable t1) {
                reply(new MessageBuilder().setContent(content).setEmbeds(embed).build());
            }
        }
    }
}
