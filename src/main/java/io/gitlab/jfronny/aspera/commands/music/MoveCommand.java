package io.gitlab.jfronny.aspera.commands.music;

import io.gitlab.jfronny.aspera.util.commands.Command;
import io.gitlab.jfronny.aspera.util.commands.CommandTag;
import io.gitlab.jfronny.aspera.util.commands.Parameter;
import io.gitlab.jfronny.aspera.util.music.TrackWrapper;

public class MoveCommand extends Command {
    @Parameter(description = "From where to get the song", required = true)
    public Integer from;
    @Parameter(description = "Where to insert the song", required = true)
    public Integer to;

    public MoveCommand() throws Exception {
        super("move", "Moves a command in the queue from one position to another", CommandTag.MusicOnly);
    }

    @Override
    public void execute() {
        from--;
        to--;
        if (from >= music.tracks.size() || from < 0)
            reply("Can't take that track: Doesn't exist");
        if (to >= music.tracks.size() || to < 0)
            reply("Can't put the track there: Invalid position");
        if (to > from) to--;
        TrackWrapper track = music.tracks.remove(from.intValue());
        music.tracks.add(to, track);
        music.refreshUI();
        reply("Moved " + track.getInfo().title + "!");
    }
}
