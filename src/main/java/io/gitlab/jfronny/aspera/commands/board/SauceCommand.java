package io.gitlab.jfronny.aspera.commands.board;

import io.gitlab.jfronny.aspera.Main;
import io.gitlab.jfronny.aspera.util.commands.Command;
import io.gitlab.jfronny.aspera.util.commands.CommandTag;
import io.gitlab.jfronny.aspera.util.commands.Parameter;
import io.gitlab.jfronny.aspera.util.scrape.saucenao.NaoScraper;
import io.gitlab.jfronny.aspera.util.scrape.saucenao.ScrapeResult;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.MessageBuilder;
import org.apache.http.util.TextUtils;

public class SauceCommand extends Command {
    @Parameter(description = "The URL of the image to look up", required = true)
    public String url;

    public SauceCommand() throws Exception {
        super("sauce", "Get the sauce of an image", CommandTag.NsfwOnly);
    }

    @Override
    public void execute() throws Exception {
        ScrapeResult result = NaoScraper.scrape(url);
        Main.LOGGER.info(result.toString());
        EmbedBuilder eb = new EmbedBuilder();
        if (!TextUtils.isEmpty(result.url))
            eb.setTitle(result.name, result.url);
        else
            eb.setTitle(result.name);
        reply(new MessageBuilder().setContent(result.naoUrl).setEmbeds(eb.setDescription(result.certitude + " " + result.estTime).build()).build());
    }
}
