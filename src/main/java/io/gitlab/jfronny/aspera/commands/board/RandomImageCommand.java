package io.gitlab.jfronny.aspera.commands.board;

import io.gitlab.jfronny.aspera.Main;
import io.gitlab.jfronny.aspera.util.commands.Command;
import io.gitlab.jfronny.aspera.util.commands.Parameter;
import io.gitlab.jfronny.aspera.util.scrape.nori.Image;
import io.gitlab.jfronny.aspera.util.scrape.nori.SearchResult;
import io.gitlab.jfronny.aspera.util.scrape.nori.ServiceTypeResolver;
import io.gitlab.jfronny.aspera.util.scrape.nori.Tag;
import io.gitlab.jfronny.aspera.util.scrape.nori.clients.SearchClient;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.MessageBuilder;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedHashMap;
import java.util.Map;

public class RandomImageCommand extends Command {
    @Parameter(description = "The booru to take a random image from, leave empty to list available", required = false, valueSupplier = "clients")
    public SearchClient booru;

    @Parameter(description = "Tags to search the booru with", required = false)
    public String tags;

    public static final Map<String, SearchClient> clients = new LinkedHashMap<>();

    static {
        for (Map.Entry<String, String> entry : Main.getConfig().boorus.entrySet()) {
            try {
                Main.LOGGER.info("Initializing booru: " + entry.getKey());
                clients.put(entry.getKey(), ServiceTypeResolver.discoverClient(URI.create(entry.getValue())).client());
            } catch (Exception e) {
                Main.LOGGER.error("Could not instantiate booru: " + entry.getKey(), e);
            }
        }
    }
    public RandomImageCommand() throws Exception {
        super("random", "Shows a random image from a booru");
    }

    @Override
    public void execute() {
        if (booru == null) {
            StringBuilder msg = new StringBuilder();
            msg.append("```\nThe booru you specified isn't available.\nChoose one of these:\n");
            for (Map.Entry<String, SearchClient> entry : clients.entrySet()) {
                msg.append(entry.getKey()).append('\n');
            }
            msg.append("```");
            reply(msg.toString());
            return;
        }

        try {
            SearchResult result = booru.search(tags == null ? booru.getDefaultQuery() : tags);
            Image randomImg = result.getImages()[Main.RNG.nextInt(result.getImages().length)];
            StringBuilder description = new StringBuilder("Tags: ");
            String sep = "";
            for (Tag tag : randomImg.tags) {
                description.append(sep).append(tag.getName());
                sep = ", ";
            }
            if (args.isNsfwAllowed() || randomImg.safeSearchRating == Image.SafeSearchRating.S) {
                reply(new MessageBuilder().setEmbeds(new EmbedBuilder().setImage(randomImg.fileUrl)
                                .setTitle(randomImg.source, randomImg.webUrl)
                                .setDescription(description)
                                .build())
                        .build());
            }
            else {
                reply("A random image was chosen but can't be sent in this non-NSFW channel");
            }
        } catch (IOException | URISyntaxException e) {
            Main.LOGGER.error("Could not execute BooruCommand", e);
            reply("Could not execute, please ask the developer for assistance");
        }
    }
    //TODO implement scheduled (cron-like) sending
    //TODO support nhentai
}
