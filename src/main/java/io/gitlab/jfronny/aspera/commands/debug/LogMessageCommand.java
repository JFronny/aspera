package io.gitlab.jfronny.aspera.commands.debug;

import io.gitlab.jfronny.aspera.Main;
import io.gitlab.jfronny.aspera.util.commands.Command;
import io.gitlab.jfronny.aspera.util.commands.CommandTag;
import io.gitlab.jfronny.aspera.util.commands.Parameter;

public class LogMessageCommand extends Command {
    @Parameter(description = "The text to print", required = true)
    public String message;

    public LogMessageCommand() throws Exception {
        super("log", "Logs the content of a message to stdout. For debugging", CommandTag.AuthorOnly);
    }

    @Override
    public void execute() {
        Main.LOGGER.info(message);
        reply("Logged.");
    }
}
