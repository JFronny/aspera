package io.gitlab.jfronny.aspera.commands.music;

import io.gitlab.jfronny.aspera.util.commands.Command;
import io.gitlab.jfronny.aspera.util.commands.CommandTag;
import io.gitlab.jfronny.aspera.util.music.MusicStateHandler;
import io.gitlab.jfronny.aspera.util.music.TrackWrapper;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.entities.Emoji;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.events.interaction.ButtonClickEvent;
import net.dv8tion.jda.api.interactions.components.ActionRow;
import net.dv8tion.jda.api.interactions.components.Button;
import net.dv8tion.jda.api.interactions.components.Component;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;

public class QueueCommand extends Command {
    public QueueCommand() throws Exception {
        super("queue", "Displays the current queue", CommandTag.MusicOnly);
    }

    @Override
    public void execute() {
        if (!music.isConnected() || music.getPlaying() == null) {
            reply("Not playing anything.");
            return;
        }

        reply(getPage(0));
    }

    @Override
    public void handleButtonClick(ButtonClickEvent event, String id) throws Exception {
        if (!music.isConnected()) {
            event.editMessage("Not playing anything.").queue();
            return;
        }
        try {
            event.editMessage(getPage(Integer.parseInt(id))).queue();
        }
        catch (NumberFormatException e) {
            event.editMessage("Something went wrong. Sorry").queue();
        }
    }

    private Message getPage(int page) {
        MessageBuilder msg = new MessageBuilder(getPageText(music, page));
        Collection<Component> actions = new LinkedHashSet<>();
        if (page > 0)
            actions.add(Button.primary("music.queue." + (page - 1), Emoji.fromMarkdown("⏮")));
        if (page < (music.tracks.size() - 1) / 10)
            actions.add(Button.primary("music.queue." + (page + 1), Emoji.fromMarkdown("⏭")));
        if (!actions.isEmpty())
            msg.setActionRows(ActionRow.of(actions));
        return msg.build();
    }

    public static String getPageText(MusicStateHandler music, int page) {
        StringBuilder replyText = new StringBuilder();
        List<TrackWrapper> tracks = music.tracks;
        replyText.append("```");
        if (page == 0 && music.getPlaying() != null) {
            replyText.append('\n').append("Currently playing: ").append(music.getDisplayName(music.getPlaying()));
            if (music.isPaused())
                replyText.append(" (paused)");
        }
        if (tracks.isEmpty())
            replyText.append('\n').append("Queue is empty.");
        else if (tracks.size() < page * 10)
            replyText.append('\n').append("You are already past the existing pages.");
        else {
            int start = page * 10;
            List<TrackWrapper> sub = tracks.subList(start, Math.min(start + 10, tracks.size()));
            for (int i = 0; i < sub.size() && i < 9; i++) {
                replyText.append('\n')
                        .append(start + i + 1)
                        .append(": ")
                        .append(music.getDisplayName(sub.get(i)));
            }
        }
        replyText.append("\n```");
        return replyText.toString();
    }
}
