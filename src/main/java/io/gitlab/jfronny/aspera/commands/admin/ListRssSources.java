package io.gitlab.jfronny.aspera.commands.admin;

import io.gitlab.jfronny.aspera.Main;
import io.gitlab.jfronny.aspera.database.data.GuildData;
import io.gitlab.jfronny.aspera.util.commands.Command;
import io.gitlab.jfronny.aspera.util.commands.CommandTag;

import java.io.IOException;

public class ListRssSources extends Command {
    public ListRssSources() throws Exception {
        super("list-rss-sources", "Lists RSS sources in the current channel", CommandTag.GuildOnly);
    }

    @Override
    public void execute() throws IOException {
        StringBuilder sources = new StringBuilder("```\nCurrent sources:\n");
        int i = 1;
        for (String source : Main.getDatabase().getGuild(guild).getRssConfigs().getOrDefault(channel.getId(), new GuildData.RssConfig()).getSources()) {
            sources.append('[').append(i++).append("] ").append(source).append('\n');
        }
        reply(sources.append("```").toString());
    }
}
