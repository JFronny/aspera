package io.gitlab.jfronny.aspera.commands.music;

import io.gitlab.jfronny.aspera.util.commands.Command;
import io.gitlab.jfronny.aspera.util.commands.CommandTag;
import io.gitlab.jfronny.aspera.util.commands.Parameter;
import io.gitlab.jfronny.aspera.util.music.TrackWrapper;
import net.dv8tion.jda.api.entities.GuildVoiceState;

public class PlayCommand extends Command {
    @Parameter(description = "The query", required = false)
    public String query;

    public PlayCommand() throws Exception {
        super("play", "Play something", CommandTag.MusicOnly);
    }

    @Override
    public void execute() {
        if (!music.isConnected()) {
            GuildVoiceState state = args.getUserVoiceState();
            if (state != null && state.inVoiceChannel()) {
                music.connect(state.getChannel());
            }
            else {
                reply("Hey, you can only use this in a voice channel");
                return;
            }
        }
        if (query == null) {
            if (music.isPaused()) {
                music.setPaused(false, this);
            }
            else {
                reply("You must provide a song to play");
            }
            return;
        }
        music.search(query, user, (songs, name) -> {
            int s = songs.size();
            reply(switch (s) {
                case 0 -> "No matches found";
                case 1 -> "Playing " + name;
                default -> "Queuing " + s + " items from " + name;
            });
            for (TrackWrapper song : songs) {
                music.queue(song, this);
            }
        }, this);
    }
}
