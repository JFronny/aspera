package io.gitlab.jfronny.aspera.commands.music;

import io.gitlab.jfronny.aspera.util.commands.Command;
import io.gitlab.jfronny.aspera.util.commands.CommandTag;
import io.gitlab.jfronny.aspera.util.music.TrackWrapper;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.MessageBuilder;

public class SongInfoCommand extends Command {
    public SongInfoCommand() throws Exception {
        super("info", "Displays information about the current song", CommandTag.MusicOnly);
    }

    @Override
    public void execute() {
        TrackWrapper at = music.getPlaying();
        if (!music.isConnected() || at == null) {
            reply("Not playing anything.");
            return;
        }
        reply(new MessageBuilder().setEmbeds(new EmbedBuilder()
                        .setTitle(at.getInfo().title, at.getInfo().uri)
                        .setAuthor(at.getInfo().author)
                .build()).build());
    }
}
