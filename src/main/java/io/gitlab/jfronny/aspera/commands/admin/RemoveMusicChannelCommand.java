package io.gitlab.jfronny.aspera.commands.admin;

import io.gitlab.jfronny.aspera.Main;
import io.gitlab.jfronny.aspera.database.data.GuildData;
import io.gitlab.jfronny.aspera.util.commands.Command;
import io.gitlab.jfronny.aspera.util.commands.CommandTag;
import io.gitlab.jfronny.aspera.util.commands.Parameter;
import io.gitlab.jfronny.aspera.util.music.MusicChannelMessageHandler;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.MessageChannel;

import java.io.IOException;

public class RemoveMusicChannelCommand extends Command {
    @Parameter(description = "The channel to return from being a music channel", required = true)
    public MessageChannel channel;

    public RemoveMusicChannelCommand() throws Exception {
        super("remove-music-channel", "Removes a text channel from the list of known music channels, disables music channel features", CommandTag.AdminOnly, CommandTag.GuildOnly);
    }

    @Override
    public void execute() {
        try {
            if (!args.callerHasPermission(Permission.ADMINISTRATOR)) {
                reply("Only administrators can perform this action!");
                return;
            }
            GuildData data = Main.getDatabase().getGuild(guild);
            if (!data.getMusicChannels().contains(channel.getId())) {
                reply("This channel was already not a music channel");
                return;
            }
            data.getMusicChannels().remove(channel.getId());
            Main.getDatabase().setGuild(data);
            MusicChannelMessageHandler.refreshUI(guild);
            reply("Done!");
        } catch (IOException e) {
            reply("Could not set channel");
            Main.LOGGER.error("Could not set music channel", e);
        }
    }
}
