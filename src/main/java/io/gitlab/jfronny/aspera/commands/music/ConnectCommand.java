package io.gitlab.jfronny.aspera.commands.music;

import io.gitlab.jfronny.aspera.util.commands.Command;
import io.gitlab.jfronny.aspera.util.commands.CommandTag;
import net.dv8tion.jda.api.entities.GuildVoiceState;

public class ConnectCommand extends Command {
    public ConnectCommand() throws Exception {
        super("connect", "Force the bot to connect to the voice channel you are in", CommandTag.MusicOnly);
    }

    @Override
    public void execute() {
        GuildVoiceState state = args.getUserVoiceState();
        if (state != null && state.inVoiceChannel()) {
            if (music.isConnected()) {
                reply("Already in a channel");
            }
            else {
                music.connect(state.getChannel());
                reply("Connected!");
            }
        }
        else {
            reply("Hey, you can only use this in a voice channel");
        }
    }
}
