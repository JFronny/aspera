package io.gitlab.jfronny.aspera;

import io.gitlab.jfronny.aspera.commands.PostCommand;
import io.gitlab.jfronny.aspera.commands.admin.*;
import io.gitlab.jfronny.aspera.commands.board.InspirobotCommand;
import io.gitlab.jfronny.aspera.commands.board.RandomImageCommand;
import io.gitlab.jfronny.aspera.commands.board.RedditCommand;
import io.gitlab.jfronny.aspera.commands.board.SauceCommand;
import io.gitlab.jfronny.aspera.commands.debug.ShutdownCommand;
import io.gitlab.jfronny.aspera.util.DbChannelCleanScheduledTask;
import io.gitlab.jfronny.aspera.util.anon.AnonChannelMessageHandler;
import io.gitlab.jfronny.aspera.util.commands.Command;
import io.gitlab.jfronny.aspera.util.commands.CommandTag;
import io.gitlab.jfronny.aspera.commands.debug.LogMessageCommand;
import io.gitlab.jfronny.aspera.commands.music.*;
import io.gitlab.jfronny.aspera.util.commands.DMCommandMessageHandler;
import io.gitlab.jfronny.aspera.util.MessageHandler;
import io.gitlab.jfronny.aspera.util.music.MusicChannelMessageHandler;
import io.gitlab.jfronny.aspera.util.music.MusicStateHandler;
import io.gitlab.jfronny.aspera.util.commands.slash.CommandManager;
import io.gitlab.jfronny.aspera.util.commands.CommandGroup;
import io.gitlab.jfronny.aspera.commands.HelpCommand;
import io.gitlab.jfronny.aspera.util.commands.slash.SlashCommandExecutionArgs;
import io.gitlab.jfronny.aspera.util.rss.RssScheduledTask;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.SelfUser;
import net.dv8tion.jda.api.events.ReadyEvent;
import net.dv8tion.jda.api.events.guild.voice.GuildVoiceLeaveEvent;
import net.dv8tion.jda.api.events.interaction.ButtonClickEvent;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.events.message.priv.PrivateMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.jetbrains.annotations.NotNull;

import java.io.Closeable;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class AsperaBot extends ListenerAdapter implements Closeable {
    private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
    private final Set<Command> commands = new LinkedHashSet<>();
    private final Set<MessageHandler> messageHandlers = new LinkedHashSet<>();
    private final Set<ScheduledFuture<?>> schedules = new HashSet<>();
    private CommandManager backend;
    private SelfUser account;
    private JDA api;

    public void setApi(JDA api) {
        this.api = api;
        api.setRequiredScopes("applications.commands");
        account = api.getSelfUser();
        backend = new CommandManager(api, Main.getConfig().botTestServer);
        Main.LOGGER.info("Initialization complete, nec aspera terrent");
    }

    public JDA getApi() {
        return api;
    }

    public CommandManager getBackend() {
        return backend;
    }

    public SelfUser getAccount() {
        return account;
    }

    public Set<Command> getCommands() {
        return commands;
    }

    public AsperaBot() throws Exception {
        try {
            commands.add(new HelpCommand());
            commands.add(new CommandGroup("music", "Command group for music playing",
                    new ConnectCommand(), new DisconnectCommand(), new PlayCommand(), new PauseCommand(),
                    new MixCommand(), new SongInfoCommand(), new MoveCommand(),
                    new SkipCommand(), new ShuffleCommand(), new ClearCommand(), new QueueCommand()));
            commands.add(new CommandGroup("board", "Provides commands for getting images from around the web",
                    new RandomImageCommand(), new InspirobotCommand(), new RedditCommand(), new SauceCommand()));
            commands.add(new CommandGroup("admin", "Administrative commands",
                    new AddMusicChannelCommand(), new RemoveMusicChannelCommand(),
                    new AddAnonChannelCommand(), new RemoveAnonChannelCommand(),
                    new AddRssSource(), new RemoveRssSource(), new ListRssSources()));
            commands.add(new CommandGroup("debug", "Commands for debugging Aspera",
                    new LogMessageCommand(), new ShutdownCommand())
                    .addTag(CommandTag.AdminOnly));
            commands.add(new PostCommand());
            messageHandlers.add(new MusicChannelMessageHandler());
            messageHandlers.add(new AnonChannelMessageHandler());
            messageHandlers.add(new DMCommandMessageHandler());
        } catch (Exception e) {
            Main.LOGGER.error("Could not initialize commands", e);
            throw new Exception("Could not initialize commands", e);
        }
        if (Main.getConfig().botOwner == null) {
            Main.LOGGER.info("botOwner is not set, this will disable debug commands");
        }
    }

    @Override
    public synchronized void onReady(@NotNull ReadyEvent event) {
        try {
            backend.initialize();
            try {
                for (Command command : commands) {
                    backend.registerCommand(command, null);
                }
            } catch (Exception e) {
                Main.LOGGER.error("Failed to register commands to discord", e);
            }
            backend.finalizeInit();
            for (ScheduledFuture<?> schedule : schedules) {
                schedule.cancel(true);
            }
            schedules.clear();
            schedules.add(scheduler.scheduleAtFixedRate(new RssScheduledTask(), 0, 60 * 30, TimeUnit.SECONDS));
            schedules.add(scheduler.scheduleAtFixedRate(new DbChannelCleanScheduledTask(), 0, 60 * 60 * 2, TimeUnit.SECONDS));
        } catch (Exception e) {
            Main.LOGGER.error("Could not finish initializing CommandManager", e);
        }
    }

    @Override
    public synchronized void onSlashCommand(@NotNull SlashCommandEvent event) {
        event.deferReply().queue();
        Command c;
        try {
            c = backend.getCommand(event.getName(), event.getGuild());
        } catch (Exception e) {
            Main.LOGGER.error("Could not identify slash command", e);
            event.getHook().sendMessage("Something went wrong on our side. Please inform this instances admin").queue();
            return;
        }

        c.execute(new SlashCommandExecutionArgs(event));
    }

    @Override
    public synchronized void onButtonClick(@NotNull ButtonClickEvent event) {
        String[] split = event.getComponentId().split("\\.", 2);
        if (split.length > 2 || split.length <= 0)
            Main.LOGGER.error("ID is formatted invalidly");
        for (MessageHandler handler : messageHandlers) {
            if (handler.getId().equals(split[0])) {
                try {
                    handler.handleButtonClick(event, split.length == 2 ? split[1] : "");
                } catch (Exception e) {
                    Main.LOGGER.error("Could not handle button click", e);
                }
                return;
            }
        }
        try {
            backend.getCommand(split[0], event.getGuild()).handleButtonClick(event, split.length == 2 ? split[1] : "");
        } catch (Exception e) {
            Main.LOGGER.error("Could not handle button click", e);
        }
    }

    @Override
    public synchronized void onGuildMessageReceived(@NotNull GuildMessageReceivedEvent event) {
        //if (event.isWebhookMessage() || event.getAuthor().isBot()) return;
        for (MessageHandler handler : messageHandlers) {
            handler.handleMessage(event);
        }
    }

    @Override
    public void onPrivateMessageReceived(@NotNull PrivateMessageReceivedEvent event) {
        if (event.getAuthor().isBot()) return;
        for (MessageHandler handler : messageHandlers) {
            handler.handleMessage(event);
        }
    }

    @Override
    public void onGuildVoiceLeave(@NotNull GuildVoiceLeaveEvent event) {
        if (event.getChannelLeft().getMembers().size() <= 1)
            MusicStateHandler.getHandler(event.getGuild()).disconnect();
    }

    @Override
    public void close() throws IOException {
        for (ScheduledFuture<?> schedule : schedules) {
            schedule.cancel(true);
        }
        api.shutdownNow();
        scheduler.shutdownNow();
    }
}
