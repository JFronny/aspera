package io.gitlab.jfronny.aspera.database.couch;

import io.gitlab.jfronny.aspera.database.data.GuildData;
import org.ektorp.CouchDbConnector;
import org.ektorp.support.CouchDbRepositorySupport;

public class GuildDataRepository extends CouchDbRepositorySupport<GuildData> {
    protected GuildDataRepository(CouchDbConnector db) {
        super(GuildData.class, db);
    }
}
