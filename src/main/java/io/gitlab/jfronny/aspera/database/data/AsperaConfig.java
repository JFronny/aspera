package io.gitlab.jfronny.aspera.database.data;

import io.gitlab.jfronny.aspera.util.scrape.nori.clients.Flickr;

import java.util.LinkedHashMap;
import java.util.Map;

public class AsperaConfig {
    public String botToken = "";
    public String botTestServer = null;
    public String botOwner = null;
    public Integer httpTimeout = 10000;
    public DbType dbType = DbType.JsonDB;
    public Map<String, String> boorus = new LinkedHashMap<>();

    public AsperaConfig() {
        boorus.put("AllTheFallen", "https://booru.allthefallen.moe");
        boorus.put("Danbooru", "https://danbooru.donmai.us");
        boorus.put("Flickr", Flickr.FLICKR_API_ENDPOINT.toString());
        boorus.put("GelBooru", "https://gelbooru.com");
        boorus.put("KonaChan", "https://konachan.com");
        boorus.put("LoliBooru", "https://lolibooru.moe");
        boorus.put("RealBooru", "https://realbooru.com");
        boorus.put("Rule34", "https://rule34.xxx");
        boorus.put("SafeBooru", "https://safebooru.org");
        boorus.put("SakugaBooru", "https://sakugabooru.com");
        boorus.put("XBooru", "https://xbooru.com");
        boorus.put("YandeRe", "https://yande.re");
    }

    public CouchDbOptions couchDb = new CouchDbOptions();

    public static class CouchDbOptions {
        public String url = "http://localhost:5984";
        public String username;
        public String password;
    }

    public enum DbType {
        CouchDB,
        JsonDB
    }
}
