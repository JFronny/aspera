package io.gitlab.jfronny.aspera.database.couch;

import io.gitlab.jfronny.aspera.Main;
import io.gitlab.jfronny.aspera.database.data.AsperaConfig;
import io.gitlab.jfronny.aspera.database.DbProvider;
import io.gitlab.jfronny.aspera.database.data.GuildData;
import org.ektorp.CouchDbInstance;
import org.ektorp.http.HttpClient;
import org.ektorp.http.StdHttpClient;
import org.ektorp.impl.StdCouchDbConnector;
import org.ektorp.impl.StdCouchDbInstance;
import org.ektorp.impl.StdObjectMapperFactory;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Set;

public class CouchDbProvider extends DbProvider {
    private final GuildDataRepository guildDb;

    public CouchDbProvider(AsperaConfig.CouchDbOptions options) {
        HttpClient httpClient;
        try {
            httpClient = new StdHttpClient.Builder()
                    .url(options.url)
                    .username(options.username)
                    .password(options.password)
                    .build();
        } catch (MalformedURLException e) {
            Main.LOGGER.error("Could not connect to DB", e);
            guildDb = null;
            return;
        }
        CouchDbInstance dbInstance = new StdCouchDbInstance(httpClient);
        guildDb = new GuildDataRepository(new StdCouchDbConnector("asperaGuilds", dbInstance));
    }

    @Override
    public GuildData getGuild(String guild) throws IOException {
        if (!guildDb.contains(guild)) {
            GuildData gd = new GuildData();
            gd.setId(guild);
            setGuild(gd);
        }
        return guildDb.get(guild);
    }

    @Override
    public void setGuild(GuildData data) throws IOException {
        guildDb.update(data);
    }

    @Override
    public void removeGuild(GuildData data) throws IOException {
        guildDb.remove(data);
    }

    @Override
    public Set<GuildData> getGuilds() throws IOException {
        return Set.copyOf(guildDb.getAll());
    }
}
