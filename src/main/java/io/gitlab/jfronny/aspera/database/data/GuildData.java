package io.gitlab.jfronny.aspera.database.data;

import com.fasterxml.jackson.core.JsonProcessingException;
import io.gitlab.jfronny.aspera.Main;
import org.ektorp.support.CouchDbDocument;

import java.util.*;

public class GuildData extends CouchDbDocument {
    private Set<String> musicChannels = new LinkedHashSet<>();
    private Set<String> anonChannels = new LinkedHashSet<>();
    private Map<String, RssConfig> rssConfigs = new HashMap<>();

    public void setMusicChannel(String s) {
        musicChannels.clear();
        musicChannels.add(s);
    }

    public void setMusicChannels(Set<String> s) {
        musicChannels = new LinkedHashSet<>(s);
    }

    public Set<String> getMusicChannels() {
        return musicChannels;
    }

    public void setAnonChannels(Set<String> anonChannels) {
        this.anonChannels = new LinkedHashSet<>(anonChannels);
    }

    public Set<String> getAnonChannels() {
        return anonChannels;
    }

    public void setRssConfigs(Map<String, RssConfig> rssConfigs) {
        this.rssConfigs = new HashMap<>(rssConfigs);
    }

    public Map<String, RssConfig> getRssConfigs() {
        return rssConfigs;
    }

    @Override
    public String toString() {
        try {
            return Main.JACKSON.writeValueAsString(this);
        } catch (JsonProcessingException e) {
            Main.LOGGER.error("Could not create JSON from guild data", e);
            return "COUNDN'T READ GUILD DATA";
        }
    }

    public static class RssConfig {
        private List<String> sources = new ArrayList<>();
        private Set<String> knownGuids = new HashSet<>();

        public List<String> getSources() {
            return sources;
        }

        public void setSources(List<String> sources) {
            this.sources = new ArrayList<>(sources);
        }

        public Set<String> getKnownGuids() {
            return knownGuids;
        }

        public void setKnownGuids(Set<String> knownGuids) {
            this.knownGuids = new HashSet<>(knownGuids);
        }
    }
}
