package io.gitlab.jfronny.aspera.database.json;

import io.gitlab.jfronny.aspera.Main;
import io.gitlab.jfronny.aspera.database.data.AsperaConfig;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Set;

import static io.gitlab.jfronny.aspera.Main.JACKSON;
import static io.gitlab.jfronny.aspera.Main.LOGGER;

public class JsonTool {
    public static final Set<String> CONFIG_FILE_NAMES = Set.of("global.json", "aspera.json", "aspera.conf");
    public static AsperaConfig getAsperaConfig() throws IOException {
        for (String knownConfigName : CONFIG_FILE_NAMES) {
            Path p = Path.of(knownConfigName);
            if (Files.exists(p))
                return getAsperaConfig(p);
        }
        for (String knownConfigName : CONFIG_FILE_NAMES) {
            Path p = Path.of("db/" + knownConfigName);
            if (Files.exists(p))
                return getAsperaConfig(p);
        }
        return getAsperaConfig(Path.of("db/aspera.json"));
    }

    private static AsperaConfig getAsperaConfig(Path path) throws IOException {
        return setData(path, getData(path, AsperaConfig.class));
    }

    public static <T> T getData(Path path, Class<T> type) throws IOException {
        if (!Files.exists(path)) {
            LOGGER.info("Generating config: " + path);
            try {
                setData(path, type.getConstructor().newInstance());
            } catch (Exception e) {
                Main.LOGGER.error("Could not get constructor for default data");
            }
        }
        try (BufferedReader reader = Files.newBufferedReader(path)) {
            return JACKSON.readValue(reader, type);
        } catch (IOException e) {
            throw new IOException("Could not get config", e);
        }
    }

    public static <T> T setData(Path path, T data) throws IOException {
        try (BufferedWriter writer = Files.newBufferedWriter(path)) {
            JACKSON.writeValue(writer, data);
        } catch (IOException e) {
            throw new IOException("Could not create empty config", e);
        }
        return data;
    }
}
