package io.gitlab.jfronny.aspera.database;

import io.gitlab.jfronny.aspera.database.data.GuildData;
import net.dv8tion.jda.api.entities.Guild;

import java.io.IOException;
import java.util.Set;

public abstract class DbProvider {
    public abstract GuildData getGuild(String guild) throws IOException;
    public abstract void setGuild(GuildData data) throws IOException;
    public abstract void removeGuild(GuildData data) throws IOException;
    public abstract Set<GuildData> getGuilds() throws IOException;

    public GuildData getGuild(Guild guild) throws IOException {
        return getGuild(guild.getId());
    }
}
