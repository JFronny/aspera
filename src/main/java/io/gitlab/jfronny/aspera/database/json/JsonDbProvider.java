package io.gitlab.jfronny.aspera.database.json;

import io.gitlab.jfronny.aspera.Main;
import io.gitlab.jfronny.aspera.database.DbProvider;
import io.gitlab.jfronny.aspera.database.data.GuildData;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.HashSet;
import java.util.Set;

import static io.gitlab.jfronny.aspera.database.json.JsonTool.getData;
import static io.gitlab.jfronny.aspera.database.json.JsonTool.setData;

public final class JsonDbProvider extends DbProvider {
    private final Path basePath;

    public JsonDbProvider(Path basePath) {
        this.basePath = basePath;
        if (!Files.exists(basePath)) try {
            Files.createDirectories(basePath);
        } catch (IOException e) {
            Main.LOGGER.error("Could not create db path", e);
        }
    }

    @Override
    public GuildData getGuild(String guild) throws IOException {
        GuildData gd = getData(getGuildPath(guild).resolve("general.json"), GuildData.class);
        gd.setId(guild);
        return gd;
    }

    @Override
    public void setGuild(GuildData data) throws IOException {
        setData(getGuildPath(data.getId()).resolve("general.json"), data);
    }

    @Override
    public void removeGuild(GuildData data) throws IOException {
        Files.walkFileTree(getGuildPath(data.getId()), new SimpleFileVisitor<>() {
            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                Files.delete(file);
                return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
                Files.delete(dir);
                return FileVisitResult.CONTINUE;
            }
        });
    }

    @Override
    public Set<GuildData> getGuilds() throws IOException {
        Set<GuildData> data = new HashSet<>();
        for (Path path : Files.list(basePath).toList()) {
            if (Files.isDirectory(path)) {
                if (isNumeric(path.getFileName().toString()))
                    data.add(getGuild(path.getFileName().toString()));
            }
        }
        return Set.copyOf(data);
    }

    private static boolean isNumeric(String str) {
        char[] charArray = str.toCharArray();
        for(char c:charArray) {
            if (!Character.isDigit(c))
                return false;
        }
        return true;
    }

    private Path getGuildPath(String guildId) throws IOException {
        Path guildDir = basePath.resolve(guildId);
        if (!Files.exists(guildDir))
            Files.createDirectories(guildDir);
        return guildDir;
    }
}
