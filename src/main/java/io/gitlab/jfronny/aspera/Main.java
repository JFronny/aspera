package io.gitlab.jfronny.aspera;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import io.gitlab.jfronny.aspera.database.DbProvider;
import io.gitlab.jfronny.aspera.database.couch.CouchDbProvider;
import io.gitlab.jfronny.aspera.database.data.AsperaConfig;
import io.gitlab.jfronny.aspera.database.data.GuildData;
import io.gitlab.jfronny.aspera.database.json.JsonDbProvider;
import io.gitlab.jfronny.aspera.database.json.JsonTool;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Comparator;
import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

public class Main {
    public static final ObjectMapper JACKSON = new ObjectMapper()
            .enable(SerializationFeature.INDENT_OUTPUT)
            .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    public static final Logger LOGGER = LoggerFactory.getLogger("Aspera");
    public static final Random RNG = new Random();
    private static AsperaBot botInstance;
    private static AsperaConfig asperaConfig;
    private static DbProvider database;

    public static void initializeConfigs() throws IOException {
        asperaConfig = JsonTool.getAsperaConfig();
    }

    public static void initializeDatabase() throws IOException {
        Path dbPath = Path.of("db");
        if (asperaConfig == null) {
            throw new NullPointerException("AsperaConfig is null, can't initialize database");
        } else {
            database = switch (asperaConfig.dbType) {
                case CouchDB -> new CouchDbProvider(asperaConfig.couchDb);
                case JsonDB -> new JsonDbProvider(dbPath);
            };
        }
        if (asperaConfig.dbType == AsperaConfig.DbType.CouchDB) {
            for (String name : JsonTool.CONFIG_FILE_NAMES)
                if (Files.exists(dbPath.resolve(name)))
                    Files.move(dbPath.resolve(name), Path.of(name));
            for (GuildData guild : new JsonDbProvider(dbPath).getGuilds()) {
                GuildData target = database.getGuild(guild.getId());
                target.setMusicChannels(guild.getMusicChannels());
                target.setAnonChannels(guild.getAnonChannels());
                database.setGuild(target);
            }
            for (Path path : Files.walk(dbPath).sorted(Comparator.reverseOrder()).toList()) {
                Files.delete(path);
            }
        }
    }

    public static void main(String[] args) {
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            if (botInstance != null) {
                try {
                    botInstance.close();
                } catch (IOException e) {
                    LOGGER.error("Could not close bot instance", e);
                }
            }
        }));
        try {
            initializeConfigs();
            initializeDatabase();
            botInstance = new AsperaBot();
            JDA api = JDABuilder.createDefault(asperaConfig.botToken).addEventListeners(botInstance).build();
            botInstance.setApi(api);
        } catch (Exception e) {
            Main.LOGGER.error("Could not initialize bot client", e);
        }
    }

    public static AsperaBot getBot() {
        if (botInstance == null) throw new NullPointerException();
        return botInstance;
    }

    public static void setConfig(AsperaConfig asperaConfig) {
        Main.asperaConfig = asperaConfig;
    }

    public static AsperaConfig getConfig() {
        if (asperaConfig == null) throw new NullPointerException();
        return asperaConfig;
    }

    public static void setDatabase(DbProvider database) {
        Main.database = database;
    }

    public static DbProvider getDatabase() {
        if (database == null) throw new NullPointerException();
        return database;
    }
}
