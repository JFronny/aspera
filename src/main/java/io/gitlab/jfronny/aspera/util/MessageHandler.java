package io.gitlab.jfronny.aspera.util;

import io.gitlab.jfronny.aspera.Main;
import io.gitlab.jfronny.aspera.util.commands.FileReply;
import io.gitlab.jfronny.aspera.util.commands.ReplyProvider;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.events.interaction.ButtonClickEvent;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.events.message.priv.PrivateMessageReceivedEvent;
import net.dv8tion.jda.api.requests.restaction.MessageAction;
import net.dv8tion.jda.api.utils.AttachmentOption;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.io.InputStream;

public abstract class MessageHandler {
    private final String id;

    public MessageHandler(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void handleMessage(@NotNull GuildMessageReceivedEvent event) {
    }

    public void handleMessage(@NotNull PrivateMessageReceivedEvent event) {
    }

    public void handleButtonClick(ButtonClickEvent event, String id) throws Exception {
    }

    protected ReplyProvider getReply(TextChannel channel, Guild guild) {
        if (channel == null) {
            return new ReplyProvider() {
                @Override
                public void reply(String text) {
                }

                @Override
                public void reply(Message message) {
                }

                @Override
                public FileReply reply(@NotNull InputStream data, @NotNull String fileName, @NotNull AttachmentOption... options) {
                    return FileReply.createNoop();
                }

                @Override
                public boolean isMusicChannel() {
                    return false;
                }
            };
        }
        return new ReplyProvider() {
            @Override
            public void reply(String text) {
                channel.sendMessage(text).queue();
            }

            @Override
            public void reply(Message message) {
                channel.sendMessage(message).queue();
            }

            @Override
            public FileReply reply(@NotNull InputStream data, @NotNull String fileName, @NotNull AttachmentOption... options) {
                return FileReply.create(channel.sendFile(data, fileName, options));
            }

            @Override
            public boolean isMusicChannel() {
                if (guild == null)
                    return false;
                try {
                    return Main.getDatabase().getGuild(guild).getMusicChannels().contains(channel.getId());
                } catch (IOException e) {
                    Main.LOGGER.error("Failed to check music channel status", e);
                    return false;
                }
            }
        };
    }
}
