/*
 * This file is part of nori.
 * Copyright (c) 2014-2016 Tomasz Jan Góralczyk <tomg@fastmail.uk>
 * License: GNU GPLv2
 */

package io.gitlab.jfronny.aspera.util.scrape.nori.clients;

import edu.umd.cs.findbugs.annotations.NonNull;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.util.TextUtils;
import org.jetbrains.annotations.Nullable;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Flickr SearchClient limited to searching for images from a single user.
 */
public class FlickrUser extends Flickr {
    //region Regular expression patterns
    /**
     * Regex pattern used to match Flickr user URLs.
     */
    public static final String FLICKR_USER_REGEX = "^https?:\\/\\/(?:www\\.|m\\.)?flickr\\.com\\/(?:#\\/)?photos\\/(.+?)\\/?$";
    //endregion

    //region Constructors

    /**
     * Create a new Flickr API client.
     *
     * @param name        Human-readable service name. (i.e. Flickr)
     * @param apiEndpoint API endpoint. (i.e. https://api.flickr.com/services)
     */
    public FlickrUser(String apiEndpoint) {
        super(apiEndpoint);
    }
    //endregion

    //region Service detection

    /**
     * Checks if the given URL exposes a supported API endpoint.
     *
     * @param uri URL to test.
     * @return Detected endpoint URL. null, if no supported endpoint URL was detected.
     */
    @Nullable
    public static String detectService(@NonNull URI uri) {
        if (uri.toString().matches(FLICKR_USER_REGEX))
            return "https://" + uri.getHost() + uri.getPath();
        return null;
    }
    //endregion

    //region Creating search URLs

    /**
     * Generate request URL to the search API endpoint.
     *
     * @param tags Space-separated tags.
     * @param pid  Page number (0-indexed).
     * @return URL to search results API.
     */
    @Override
    protected String createSearchURL(String tags, int pid) throws URISyntaxException {
        Pattern p = Pattern.compile(FLICKR_USER_REGEX);
        Matcher m = p.matcher(apiEndpoint.toString());

        if (m.matches()) {
            return new URIBuilder(FLICKR_API_ENDPOINT)
                    .setParameter("api_key", FLICKR_API_KEY)
                    .setParameter("user_id", m.group(1))
                    .setParameter("method", !TextUtils.isEmpty(tags) ? "flickr.photos.search" : "flickr.people.getPhotos")
                    .setParameter("text", tags != null ? tags : "")
                    .setParameter("per_page", Integer.toString(DEFAULT_LIMIT, 10))
                    .setParameter("extras", "date_upload,owner_name,media,tags,path_alias,icon_server,o_dims,path_alias,original_format,url_q,url_m,url_l,url_o")
                    .setParameter("page", Integer.toString(pid + 1, 10))
                    .build()
                    .toString();
        }
        return super.createSearchURL(tags, pid);
    }
    //endregion
}
