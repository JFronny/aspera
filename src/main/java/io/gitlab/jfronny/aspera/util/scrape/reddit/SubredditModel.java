package io.gitlab.jfronny.aspera.util.scrape.reddit;

import java.util.List;

public class SubredditModel {
    public Data data;
    public static class Data {
        public List<Child> children;
        public static class Child {
            public ChildData data;
            public static class ChildData {
                public Boolean over_18;
                public String author;
                public String subreddit_name_prefixed;
                public String title;
                public String permalink;
                public String selftext;
                public String url;
                public Media media;

                public static class Media {
                    public RedditVideo reddit_video;
                    public static class RedditVideo {
                        public String fallback_url;
                    }

                    @Override
                    public String toString() {
                        return "Media{" +
                                "reddit_video=" + reddit_video +
                                '}';
                    }
                }

                @Override
                public String toString() {
                    return "ChildData{" +
                            "over_18=" + over_18 +
                            ", author='" + author + '\'' +
                            ", subreddit_name_prefixed='" + subreddit_name_prefixed + '\'' +
                            ", title='" + title + '\'' +
                            ", permalink='" + permalink + '\'' +
                            ", selftext='" + selftext + '\'' +
                            ", url='" + url + '\'' +
                            ", media=" + media +
                            '}';
                }
            }
        }
    }
}
