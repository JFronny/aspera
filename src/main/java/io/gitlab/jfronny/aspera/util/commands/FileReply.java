package io.gitlab.jfronny.aspera.util.commands;

import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.requests.restaction.MessageAction;
import net.dv8tion.jda.api.requests.restaction.WebhookMessageAction;

import java.util.Arrays;

public interface FileReply {
    FileReply setContent(String content);
    FileReply setEmbeds(MessageEmbed... embeds);
    FileReply send();

    static FileReply create(WebhookMessageAction<Message> hook) {
        return new FileReply() {
            @Override
            public FileReply setContent(String content) {
                hook.setContent(content);
                return this;
            }

            @Override
            public FileReply setEmbeds(MessageEmbed... embeds) {
                hook.addEmbeds(Arrays.asList(embeds));
                return this;
            }

            @Override
            public FileReply send() {
                hook.queue();
                return this;
            }
        };
    }

    static FileReply create(MessageAction action) {
        return new FileReply() {
            @Override
            public FileReply setContent(String content) {
                action.content(content);
                return this;
            }

            @Override
            public FileReply setEmbeds(MessageEmbed... embeds) {
                action.setEmbeds(embeds);
                return this;
            }

            @Override
            public FileReply send() {
                action.queue();
                return this;
            }
        };
    }

    static FileReply createNoop() {
        return new FileReply() {
            @Override
            public FileReply setContent(String content) {
                return this;
            }

            @Override
            public FileReply setEmbeds(MessageEmbed... embeds) {
                return this;
            }

            @Override
            public FileReply send() {
                return this;
            }
        };
    }
}
