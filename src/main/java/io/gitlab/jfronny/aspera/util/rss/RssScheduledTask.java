package io.gitlab.jfronny.aspera.util.rss;

import io.gitlab.jfronny.aspera.Main;
import io.gitlab.jfronny.aspera.database.data.GuildData;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.TextChannel;

import java.util.Map;

public class RssScheduledTask implements Runnable {
    @Override
    public void run() {
        try {
            Main.LOGGER.info("Running rss task");
            for (Guild guild : Main.getBot().getApi().getGuilds()) {
                try {
                    GuildData data = Main.getDatabase().getGuild(guild);
                    boolean changed = false;
                    for (Map.Entry<String, GuildData.RssConfig> entry : data.getRssConfigs().entrySet()) {
                        TextChannel target = guild.getTextChannelById(entry.getKey());
                        if (target == null) continue;
                        for (String source : entry.getValue().getSources()) {
                            for (RssInfo.Item item : RssParser.fetch(source).items) {
                                if (entry.getValue().getKnownGuids().add(item.guid)) {
                                    changed = true;
                                    target.sendMessage(new MessageBuilder().setEmbeds(
                                            new EmbedBuilder().setTitle(item.title, item.link).setDescription(item.description).build()
                                    ).build()).queue();
                                }
                            }
                        }
                    }
                    if (changed)
                        Main.getDatabase().setGuild(data);
                }
                catch (Throwable t) {
                    Main.LOGGER.error("Error in RssScheduledTask for " + guild.getName(), t);
                }
            }
        }
        catch (Throwable t) {
            Main.LOGGER.error("Error in RssScheduledTask", t);
        }
    }
}
