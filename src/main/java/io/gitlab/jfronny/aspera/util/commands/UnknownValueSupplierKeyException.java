package io.gitlab.jfronny.aspera.util.commands;

public class UnknownValueSupplierKeyException extends Exception {
    public UnknownValueSupplierKeyException(String fieldName) {
        super("Key not known for " + fieldName);
    }
}
