package io.gitlab.jfronny.aspera.util.anon;

import io.gitlab.jfronny.aspera.Main;
import io.gitlab.jfronny.aspera.util.MessageHandler;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageType;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.requests.restaction.MessageAction;
import net.dv8tion.jda.api.utils.AttachmentOption;
import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.function.Consumer;

public class AnonChannelMessageHandler extends MessageHandler {
    public AnonChannelMessageHandler() {
        super("anonChannel");
    }

    @Override
    public void handleMessage(@NotNull GuildMessageReceivedEvent event) {
        if (event.isWebhookMessage() || event.getAuthor().isBot()) return;
        try {
            TextChannel channel = event.getChannel();
            if (Main.getDatabase().getGuild(event.getGuild()).getAnonChannels().contains(channel.getId())) {
                Message msg = event.getMessage();
                addAttachment(msg.getAttachments(), channel.sendMessage(msg), action -> {
                    action.queue();
                    msg.delete().queue();
                });
            }
        } catch (Exception e) {
            Main.LOGGER.error("Could not check whether a message was sent in a music channel", e);
        }
    }

    private void addAttachment(List<Message.Attachment> att, MessageAction action, Consumer<MessageAction> done) {
        if (att.isEmpty())
            done.accept(action);
        else {
            final Message.Attachment attC = att.get(0);
            attC.retrieveInputStream().thenAccept(stream -> addAttachment(att.subList(1, att.size()), attC.isSpoiler()
                    ? action.addFile(stream, attC.getFileName(), AttachmentOption.SPOILER)
                    : action.addFile(stream, attC.getFileName()), done));
        }
    }
}
