package io.gitlab.jfronny.aspera.util.commands;

import io.gitlab.jfronny.aspera.Main;
import io.gitlab.jfronny.aspera.util.MessageHandler;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.events.message.priv.PrivateMessageReceivedEvent;
import net.dv8tion.jda.api.utils.AttachmentOption;
import org.jetbrains.annotations.NotNull;

import java.io.InputStream;
import java.io.InvalidClassException;
import java.util.Arrays;
import java.util.Map;

public class DMCommandMessageHandler extends MessageHandler {
    public DMCommandMessageHandler() {
        super("dmCommand");
    }

    @Override
    public void handleMessage(@NotNull PrivateMessageReceivedEvent event) {
        String[] segments = event.getMessage().getContentDisplay().split("\\s(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)");
        try {
            Command command = Main.getBot().getBackend().getCommand(segments[0], null);
            if (command instanceof CommandGroup cg) {
                Command sg = cg.getSubcommand(segments[1]);
                execute(sg, Arrays.copyOfRange(segments, 2, segments.length), event, new String[]{command.getName(), sg.getName()});
            } else
                execute(command, Arrays.copyOfRange(segments, 1, segments.length), event, new String[] {command.getName()});
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void execute(Command command, String[] params, PrivateMessageReceivedEvent event, String[] path) {
        command.execute(new StringArrayExecutionArgs(params, event, path));
    }

    private record StringArrayExecutionArgs(String[] params, PrivateMessageReceivedEvent event, String[] path) implements CommandExecutionArgs {
        @Override
        public User getUser() {
            return event.getAuthor();
        }

        @Override
        public Guild getGuild() {
            for (Guild guild : getUser().getMutualGuilds())
                for (GuildVoiceState state : guild.getVoiceStates())
                    if (state.getMember().getUser() == getUser())
                        return guild;
            return null;
        }

        @Override
        public MessageChannel getChannel() {
            return event.getChannel();
        }

        @Override
        public ReplyProvider getReply() {
            return new ReplyProvider() {
                @Override
                public void reply(String text) {
                    event.getMessage().reply(text).queue();
                }

                @Override
                public void reply(Message message) {
                    event.getMessage().reply(message).queue();
                }

                @Override
                public FileReply reply(@NotNull InputStream data, @NotNull String fileName, @NotNull AttachmentOption... options) {
                    return FileReply.create(event.getMessage().reply(data, fileName, options));
                }

                @Override
                public boolean isMusicChannel() {
                    return false;
                }
            };
        }

        @Override
        public String getSubcommandName() {
            return path.length > 1 ? path[1] : null;
        }

        @Override
        public String getCommandPath() {
            return String.join("/", path);
        }

        @Override
        public GuildVoiceState getUserVoiceState() {
            for (Guild guild : getUser().getMutualGuilds())
                for (GuildVoiceState state : guild.getVoiceStates())
                    if (state.getMember().getUser() == getUser())
                        return state;
            return null;
        }

        @Override
        public boolean callerHasPermission(Permission permission) {
            return true;
        }

        @Override
        public OptionSupplier getOptionSupplier() {
            return new StringArrayOptionSupplier();
        }

        @Override
        public boolean isNsfwAllowed() {
            return true;
        }

        @Override
        public boolean isGuild() {
            return false;
        }

        private class StringArrayOptionSupplier implements OptionSupplier {
            private int position = 0;

            private String readNext() {
                String valueRaw = params[position++];
                if (valueRaw.startsWith("\"") && valueRaw.endsWith("\""))
                    valueRaw = valueRaw.substring(1, valueRaw.length() - 1);
                return valueRaw;
            }

            @Override
            public Object nextValue(OptionSupplierParameter<?> option) {
                String valueRaw;
                try {
                    valueRaw = readNext();
                }
                catch (Exception e) {
                    if (option.parameter().isRequired()) {
                        throw new RuntimeException("Could not read next", e);
                    }
                    return null;
                }
                Class<?> targetType = option.targetType();
                Object value;
                switch (option.parameter().getOptionType()) {
                    case STRING:
                        Map<String, ?> valueSupplier = option.parameter().getValueSupplier();
                        if (valueSupplier == null)
                            value = valueRaw;
                        else {
                            if (!valueSupplier.containsKey(valueRaw))
                                getReply().reply("Could not read parameter " + option.parameter().getName() + ": invalid key");
                            value = valueSupplier.get(valueRaw);
                        }
                        break;
                    case INTEGER:
                        if (targetType.isEnum()) {
                            value = null;
                            for (Object o : targetType.getEnumConstants()) {
                                if (o.toString().toLowerCase().equals(valueRaw))
                                    value = o;
                            }
                            if (value == null) {
                                getReply().reply("Could not find value: " + valueRaw);
                            }
                        }
                        else if (targetType.isAssignableFrom(Long.class))
                            value = Long.valueOf(valueRaw);
                        else
                            value = Integer.valueOf(valueRaw);
                        break;
                    case BOOLEAN:
                        value = Boolean.valueOf(valueRaw);
                        break;
                    case CHANNEL:
                        if (GuildChannel.class.isAssignableFrom(targetType))
                            value = Main.getBot().getApi().getGuildChannelById(valueRaw);
                        else {
                            getReply().reply("Target type not valid for channel");
                            value = null;
                        }
                        break;
                    default:
                        getReply().reply("This command is not supported here!");
                        value = null;
                        break;
                }
                return value;
            }

            @Override
            public void reset() {
                position = 0;
            }
        }
    }
}
