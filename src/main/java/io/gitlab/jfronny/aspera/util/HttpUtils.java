package io.gitlab.jfronny.aspera.util;

import io.gitlab.jfronny.aspera.Main;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;
import java.util.stream.Stream;

public class HttpUtils {
    private static final HttpClient CLIENT = HttpClient.newBuilder().followRedirects(HttpClient.Redirect.ALWAYS).build();
    private static final String USER_AGENT = "Aspera/" + HttpUtils.class.getPackage().getImplementationVersion();

    private enum Method {
        GET,
        POST
    }

    public static class Request {
        private HttpRequest.Builder builder;
        private Method method;
        private final String url;
        private boolean logInfo;

        private Request(Method method, String url) {
            this.url = url;
            try {
                this.builder = HttpRequest.newBuilder().uri(new URI(url))
                        .header("User-Agent", USER_AGENT) // nori/BuildConfig.VERSION_NAME
                        .timeout(Duration.ofMillis(Main.getConfig().httpTimeout));
                this.method = method;
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
        }

        public Request bearer(String token) {
            builder.header("Authorization", "Bearer " + token);

            return this;
        }

        public Request bodyString(String string) {
            builder.header("Content-Type", "text/plain");
            builder.method(method.name(), HttpRequest.BodyPublishers.ofString(string));
            method = null;

            return this;
        }

        public Request bodyForm(String string) {
            builder.header("Content-Type", "application/x-www-form-urlencoded");
            builder.method(method.name(), HttpRequest.BodyPublishers.ofString(string));
            method = null;

            return this;
        }

        public Request bodyJson(String string) {
            builder.header("Content-Type", "application/json");
            builder.method(method.name(), HttpRequest.BodyPublishers.ofString(string));
            method = null;

            return this;
        }

        public Request logState() {
            logInfo = true;
            return this;
        }

        private <T> T _send(String accept, HttpResponse.BodyHandler<T> responseBodyHandler) {
            builder.header("Accept", accept);
            if (method != null) builder.method(method.name(), HttpRequest.BodyPublishers.noBody());

            try {
                var res = CLIENT.send(builder.build(), responseBodyHandler);
                if (logInfo)
                    Main.LOGGER.info(url + " - " + res.statusCode());
                return res.statusCode() == 200 ? res.body() : null;
            } catch (IOException | InterruptedException e) {
                if (logInfo)
                    Main.LOGGER.error("Could not fetch " + url, e);
                return null;
            }
        }

        public void send() {
            _send("*/*", HttpResponse.BodyHandlers.discarding());
        }

        public InputStream sendInputStream() {
            return _send("*/*", HttpResponse.BodyHandlers.ofInputStream());
        }

        public String sendString() {
            return _send("*/*", HttpResponse.BodyHandlers.ofString());
        }

        public Stream<String> sendLines() {
            return _send("*/*", HttpResponse.BodyHandlers.ofLines());
        }

        public boolean sendProbe() {
            try (InputStream is = sendInputStream()) {
                if (is == null)
                    return false;
                is.close();
                return true;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        }
    }

    public static Request get(String url) {
        return new Request(Method.GET, url);
    }

    public static Request post(String url) {
        return new Request(Method.POST, url);
    }

    public static String appendPath(String base, String path) throws URISyntaxException {
        if (path.startsWith("/"))
            path = path.substring(1);
        String[] sr = base.split("\\?");
        if (sr.length == 1) {
            if (sr[0].endsWith("/"))
                return sr[0].substring(0, sr[0].length() - 1) + "/" + path;
            return sr[0] + "/" + path;
        }
        else if (sr.length == 2) {
            if (sr[0].endsWith("/"))
                return sr[0].substring(0, sr[0].length() - 1) + "/" + path + "?" + sr[1];
            return sr[0] + "/" + path + "?" + sr[1];
        }
        else throw new URISyntaxException(base, "A URI must not contain multiple ?s");
    }
}
