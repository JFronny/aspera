/*
 * This file is part of nori.
 * Copyright (c) 2014 Tomasz Jan Góralczyk <tomg@fastmail.uk>
 * License: ISC
 */

package io.gitlab.jfronny.aspera.util.scrape.nori;

import org.apache.http.util.TextUtils;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Metadata received from the API for each image.
 */
public class Image {
    /**
     * Regular expression for matching Pixiv image ID from Pixiv URLs
     */
    private static final Pattern PIXIV_ID_FROM_URL_PATTERN = Pattern.compile("http://(?:www|i\\d)\\.pixiv\\.net/.+?(?:illust_id=|img/.+?/)(\\d+)");
    /**
     * Full-resolution image URL.
     */
    public String fileUrl;
    /**
     * Image width.
     */
    public int width;
    //endregion

    //region Regular expressions
    /**
     * Image height.
     */
    public int height;
    //endregion

    //region Instance fields
    /**
     * Thumbnail URL.
     */
    public String previewUrl;
    /**
     * Thumbnail width.
     */
    public int previewWidth = 0;
    /**
     * Thumbnail height
     */
    public int previewHeight = 0;
    /**
     * Sample URL.
     */
    public String sampleUrl;

    // Samples are medium-resolution images downsized for viewing on the web.
    // Usually no more than ~1000px width.
    // Suitable for slow networks and low resolution devices (mdpi or less).
    /**
     * Sample width.
     */
    public int sampleWidth = 0;
    /**
     * Sample height.
     */
    public int sampleHeight = 0;
    /**
     * Image tags.
     */
    public Tag[] tags;
    /**
     * Image ID
     */
    public String id;
    /**
     * Image parent ID. Used when there are multiple similar images.
     */
    public String parentId;
    /**
     * Parent ID
     */
    public String pixivId;
    /**
     * Web URL.
     */
    public String webUrl;
    /**
     * Source URL.
     */
    public String source;
    /**
     * MD5 hash
     */
    public String md5;
    /**
     * Search result page that contains this Image.
     */
    public Integer searchPage;
    /**
     * The position of the Image on the search result page.
     */
    public Integer searchPagePosition;
    /**
     * SafeSearch rating.
     */
    public SafeSearchRating safeSearchRating;
    /**
     * Popularity score.
     */
    public Integer score;
    /**
     * Upload date.
     */
    public Date createdAt;

    // Parcelables are the standard Android serialization API used to retain data between sessions.
    /**
     * Default constructor
     */
    public Image() {
    }

    /**
     * Extract a Pixiv ID from URL to an image's Pixiv page.
     *
     * @param url Pixiv URL.
     * @return Pixiv ID. Null if an ID could not be matched.
     */
    public static String getPixivIdFromUrl(String url) {
        // Make sure the URL isn't empty or null.
        if (url == null || url.isEmpty()) {
            return null;
        }

        // Match regular expression against URL.
        Matcher matcher = PIXIV_ID_FROM_URL_PATTERN.matcher(url);
        if (matcher.find()) {
            return matcher.group(1);
        }

        // No ID matched.
        return null;
    }
    //endregion

    //region File extension from URL

    /**
     * Attempts to *guess* the file type of the {@link Image} based on the File URL.
     * May not be accurate for API types that don't include the file extension in image file names.
     *
     * @return Lower-case file extension, without the preceding dot. "jpeg" gets normalised into
     * "jpg".
     */
    @Nullable
    public String getFileExtension() {
        String path = this.fileUrl.split("\\?")[0];
        if (path.startsWith("http://")) path = path.substring(7);
        if (path.startsWith("https://")) path = path.substring(8);
        if (!path.contains("/"))
            return null;
        path = path.split("/", 2)[1];
        path = path.substring(path.lastIndexOf('/') + 1);
        String fileExt = (!TextUtils.isEmpty(path) && path.contains(".")) ?
                path.toLowerCase(Locale.US).substring(path.lastIndexOf(".") + 1) : null;

        return "jpeg".equals(fileExt) ? "jpg" : fileExt;
    }
    //endregion

    //region SafeSearchRating enum

    /**
     * Safe-for-work ratings.
     * Users can choose to hide images with certain SafeSearch ratings.
     */
    public enum SafeSearchRating {
        /**
         * Image is safe for work.
         */
        S,
        /**
         * Image is generally safe, but may contain some suggestive content
         */
        Q,
        /**
         * Image is not safe for work.
         */
        E,
        /**
         * Rating is unknown or has not been set.
         */
        U;

        //region Static helper methods

        /**
         * Convert a String array into an array of {@link SafeSearchRating}s.
         *
         * @param strings String array.
         * @return Array of {@link SafeSearchRating}s.
         */
        public static SafeSearchRating[] arrayFromStrings(String... strings) {
            final List<SafeSearchRating> ratingList = new ArrayList<>(4);

            for (String string : strings) {
                string = string.toLowerCase(Locale.US);
                if (string.contains("f")) { // saFe
                    ratingList.add(SafeSearchRating.S);
                } else if (string.contains("q")) { // Questionable
                    ratingList.add(SafeSearchRating.Q);
                } else if (string.contains("x")) { // eXplicit
                    ratingList.add(SafeSearchRating.E);
                } else if (string.contains("u")) { // Undefined
                    ratingList.add(SafeSearchRating.U);
                }
            }

            return ratingList.toArray(new SafeSearchRating[0]);
        }

        /**
         * Get a SafeSearchRating from a raw String representation returned by the API.
         *
         * @param s String returned by the API.
         * @return SafeSearchRating for given value.
         */
        public static SafeSearchRating fromString(String s) {
            // Convert string to lower-case and look at first character only.
            return switch (s.toLowerCase(Locale.US).charAt(0)) {
                case 's' -> SafeSearchRating.S;
                case 'q' -> SafeSearchRating.Q;
                case 'e' -> SafeSearchRating.E;
                default -> SafeSearchRating.U;
            };
        }
        //endregion
    }
    //endregion
}
