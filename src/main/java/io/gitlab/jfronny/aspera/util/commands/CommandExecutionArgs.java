package io.gitlab.jfronny.aspera.util.commands;

import io.gitlab.jfronny.aspera.util.commands.slash.DiscordParameter;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.*;

public interface CommandExecutionArgs {
    User getUser();
    Guild getGuild();
    MessageChannel getChannel();
    ReplyProvider getReply();
    String getSubcommandName();
    String getCommandPath();
    GuildVoiceState getUserVoiceState();
    boolean callerHasPermission(Permission permission);
    OptionSupplier getOptionSupplier();
    boolean isNsfwAllowed();
    boolean isGuild();

    interface OptionSupplier {
        Object nextValue(OptionSupplierParameter<?> option);
        void reset();
    }

    record OptionSupplierParameter<T>(DiscordParameter parameter, Class<T> targetType) {
    }
}
