package io.gitlab.jfronny.aspera.util.scrape.saucenao;

public class ScrapeResult {
    public String certitude;
    public String url;
    public String name;
    public String estTime;
    public String naoUrl;

    @Override
    public String toString() {
        return "ScrapeResult{" +
                "certitude='" + certitude + '\'' +
                ", url='" + url + '\'' +
                ", name='" + name + '\'' +
                ", estTime='" + estTime + '\'' +
                ", naoUrl='" + naoUrl + '\'' +
                '}';
    }
}
