package io.gitlab.jfronny.aspera.util.commands.slash;

import io.gitlab.jfronny.aspera.Main;
import io.gitlab.jfronny.aspera.util.commands.CommandExecutionArgs;
import io.gitlab.jfronny.aspera.util.commands.FileReply;
import io.gitlab.jfronny.aspera.util.commands.ReplyProvider;
import io.gitlab.jfronny.aspera.util.commands.UnknownValueSupplierKeyException;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.requests.restaction.MessageAction;
import net.dv8tion.jda.api.utils.AttachmentOption;
import org.jetbrains.annotations.NotNull;

import java.io.InputStream;

public record SlashCommandExecutionArgs(SlashCommandEvent event) implements CommandExecutionArgs {
    @Override
    public User getUser() {
        return event.getUser();
    }

    @Override
    public Guild getGuild() {
        return event.getGuild();
    }

    @Override
    public MessageChannel getChannel() {
        return event.getChannel();
    }

    @Override
    public ReplyProvider getReply() {
        return new ReplyProvider() {
            @Override
            public void reply(String text) {
                event.getHook().sendMessage(text).queue();
            }

            @Override
            public void reply(Message message) {
                event.getHook().sendMessage(message).queue();
            }

            @Override
            public FileReply reply(@NotNull InputStream data, @NotNull String fileName, @NotNull AttachmentOption... options) {
                return FileReply.create(event.getHook().sendFile(data, fileName, options));
            }

            @Override
            public boolean isMusicChannel() {
                return false;
            }
        };
    }

    @Override
    public String getSubcommandName() {
        return event.getSubcommandName();
    }

    @Override
    public String getCommandPath() {
        return event().getCommandPath();
    }

    @Override
    public GuildVoiceState getUserVoiceState() {
        return event.getMember().getVoiceState();
    }

    @Override
    public boolean callerHasPermission(Permission permission) {
        return event.getMember().hasPermission(permission);
    }

    @Override
    public OptionSupplier getOptionSupplier() {
        return new OptionSupplierImpl();
    }

    @Override
    public boolean isNsfwAllowed() {
        return event.getTextChannel().isNSFW();
    }

    @Override
    public boolean isGuild() {
        return true;
    }

    private class OptionSupplierImpl implements OptionSupplier {
        @Override
        public Object nextValue(OptionSupplierParameter<?> option) {
            try {
                return option.parameter().getValue(event.getOption(option.parameter().getName()), option.targetType());
            } catch (UnknownValueSupplierKeyException e) {
                getReply().reply("Could not get argument for " + option.parameter().getName() + ": invalid key");
                return null;
            } catch (Exception e) {
                Main.LOGGER.error("Could not get argument", e);
                return null;
            }
        }

        @Override
        public void reset() {
        }
    }
}
