package io.gitlab.jfronny.aspera.util;

import io.gitlab.jfronny.aspera.Main;
import io.gitlab.jfronny.aspera.database.data.GuildData;
import io.gitlab.jfronny.aspera.util.rss.RssInfo;
import io.gitlab.jfronny.aspera.util.rss.RssParser;
import net.dv8tion.jda.api.entities.Guild;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class DbChannelCleanScheduledTask implements Runnable {
    @Override
    public void run() {
        try {
            Main.LOGGER.info("Running db cleanup task");
            for (GuildData guild : Set.copyOf(Main.getDatabase().getGuilds())) {
                Guild dGuild = Main.getBot().getApi().getGuildById(guild.getId());
                if (dGuild == null) {
                    Main.getDatabase().removeGuild(guild);
                } else {
                    boolean changed = false;
                    for (String s : Set.copyOf(guild.getMusicChannels())) {
                        if (dGuild.getTextChannelById(s) == null) {
                            guild.getMusicChannels().remove(s);
                            changed = true;
                        }
                    }
                    for (String s : Set.copyOf(guild.getAnonChannels())) {
                        if (dGuild.getTextChannelById(s) == null) {
                            guild.getAnonChannels().remove(s);
                            changed = true;
                        }
                    }
                    for (Map.Entry<String, GuildData.RssConfig> entry : guild.getRssConfigs().entrySet()) {
                        if (dGuild.getTextChannelById(entry.getKey()) == null) {
                            guild.getRssConfigs().remove(entry.getKey());
                            changed = true;
                            continue;
                        }
                        Set<String> knownGuids = new HashSet<>();
                        for (String source : entry.getValue().getSources()) {
                            try {
                                for (RssInfo.Item item : RssParser.fetch(source).items) {
                                    knownGuids.add(item.guid);
                                }
                            }
                            catch (Throwable t) {
                                Main.LOGGER.error("Could not fetch RSS source", t);
                            }
                        }
                        for (String s : Set.copyOf(entry.getValue().getKnownGuids())) {
                            if (!knownGuids.contains(s)) {
                                entry.getValue().getKnownGuids().remove(s);
                                changed = true;
                            }
                        }
                    }
                    if (changed)
                        Main.getDatabase().setGuild(guild);
                }
            }
        }
        catch (Throwable t) {
            Main.LOGGER.error("Error in DbChannelCleanScheduledTask", t);
        }
    }
}
