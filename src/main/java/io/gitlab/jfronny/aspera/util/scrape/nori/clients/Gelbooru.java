/*
 * This file is part of nori.
 * Copyright (c) 2014 Tomasz Jan Góralczyk <tomg@fastmail.uk>
 * License: ISC
 */

package io.gitlab.jfronny.aspera.util.scrape.nori.clients;

import edu.umd.cs.findbugs.annotations.NonNull;
import io.gitlab.jfronny.aspera.util.HttpUtils;
import org.jetbrains.annotations.Nullable;

import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Client for the Gelbooru API.
 * The Gelbooru API is based on the Danbooru 1.x API with a few minor differences.
 */
public class Gelbooru extends DanbooruLegacy {
    //region Constants
    /**
     * Date format used by Gelbooru.
     */
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("EEE MMM d HH:mm:ss Z yyyy", Locale.US);
    //endregion

    //region Constructors
    public Gelbooru(String endpoint) {
        super(endpoint);
    }
    //endregion

    //region Service detection

    /**
     * Checks if the given URL exposes a supported API endpoint.
     *
     * @return Detected endpoint URL. null, if no supported endpoint URL was detected.
     */
    @Nullable
    public static String detectService(@NonNull String uri) throws URISyntaxException {
        final String endpointUrl = HttpUtils.appendPath(uri, "/index.php?page=dapi&s=post&q=index");

        /*final Response<DataEmitter> response = Ion.with(context)
                .load(endpointUrl)
                .setTimeout(timeout)
                .userAgent(SearchClient.USER_AGENT)
                .followRedirect(false)
                .noCache()
                .asDataEmitter()
                .withResponse()
                .get();

        // Close the connection.
        final DataEmitter dataEmitter = response.getResult();
        if (dataEmitter != null) dataEmitter.close();

        if (response.getHeaders().code() == 200) {
            return uri.toString();
        }*/
        if (HttpUtils.get(endpointUrl).sendProbe())
            return uri;
        return null;
    }
    //endregion

    //region Creating search URLs
    @Override
    protected String createSearchURL(String tags, int pid, int limit) {
        // Unlike DanbooruLegacy, page numbers are 0-indexed for Gelbooru APIs.
        return String.format(Locale.US, "%s/index.php?page=dapi&s=post&q=index&tags=%s&pid=%d&limit=%d", apiEndpoint, URLEncoder.encode(tags, StandardCharsets.UTF_8), pid, limit);
    }
    //endregion

    //region Parsing responses
    @Override
    protected String webUrlFromId(String id) {
        return String.format(Locale.US, "%s/index.php?page=post&s=view&id=%s", apiEndpoint, id);
    }

    @Override
    protected Date dateFromString(String date) throws ParseException {
        // Override Danbooru 1.x date format.
        return DATE_FORMAT.parse(date);
    }
    //endregion
}
