package io.gitlab.jfronny.aspera.util.commands;

import io.gitlab.jfronny.aspera.Main;
import net.dv8tion.jda.api.events.interaction.ButtonClickEvent;

import java.util.LinkedHashMap;
import java.util.Map;

public class CommandGroup extends Command {
    private final Map<String, Command> subCommands = new LinkedHashMap<>();

    @Override
    public void execute() {
        Main.LOGGER.error("execute invoked directly on command group");
    }

    @Override
    public void execute(CommandExecutionArgs args) {
        if (hasSubcommand(args.getSubcommandName())) {
            try {
                getSubcommand(args.getSubcommandName()).execute(args);
            } catch (Exception e) {
                Main.LOGGER.error("Could not get subcommand", e);
                args.getReply().reply("Something went wrong on our side. Please inform this instances admin");
            }
        }
        else {
            Main.LOGGER.error("Could not find command: " + args.getCommandPath());
            args.getReply().reply("Something went wrong on our side. Please inform this instances admin");
        }
    }

    @Override
    public void handleButtonClick(ButtonClickEvent event, String id) throws Exception {
        String[] split = id.split("\\.", 2);
        if (split.length != 2)
            throw new Exception("ID is formatted invalidly");
        getSubcommand(split[0]).handleButtonClick(event, split[1]);
    }

    public CommandGroup(String name, String description, Command... subcommands) throws Exception {
        super(name, description);
        for (Command subcommand : subcommands) {
            addSubcommand(subcommand);
        }
    }

    public boolean hasSubcommand(String name) {
        return this.getSubcommands().containsKey(name);
    }

    public Command addSubcommand(Command command) throws Exception {
        if (command instanceof CommandGroup)
            throw new Exception("Stacked command groups are not currently supported");
        subCommands.put(command.getName(), command);
        return this;
    }

    public Command getSubcommand(String name) throws Exception {
        if (hasSubcommand(name)) {
            if (!subCommands.containsKey(name))
                subCommands.put(name, getSubcommands().get(name));
            return subCommands.get(name);
        }
        throw new Exception("Not found");
    }

    public Map<String, Command> getSubcommands() {
        return subCommands;
    }
}
