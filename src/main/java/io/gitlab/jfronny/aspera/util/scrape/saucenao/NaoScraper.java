package io.gitlab.jfronny.aspera.util.scrape.saucenao;

import io.gitlab.jfronny.aspera.Main;
import io.gitlab.jfronny.aspera.util.HttpUtils;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

public class NaoScraper {
    public static ScrapeResult scrape(String imageUrl) {
        ScrapeResult result = new ScrapeResult();
        result.naoUrl = "https://saucenao.com/search.php?db=999&url=" + URLEncoder.encode(imageUrl, StandardCharsets.UTF_8);
        String sauceSite = HttpUtils.get(result.naoUrl).sendString();
        sauceSite = substring(sauceSite, "<div class=\"resultimage\" >", "<div class=\"resultimage\" >");
        result.certitude = substring(sauceSite, "<div class=\"resultsimilarityinfo\">", "</div>");
        sauceSite = substring(sauceSite, "<div class=\"resultmiscinfo\">", null);
        result.url = substring(sauceSite, "<a href=\"", "\"");
        sauceSite = substring(sauceSite, "<div class=\"resultcontent\">", null);
        result.name = substring(sauceSite, "<div class=\"resulttitle\">", "</div>")
                .replace("<strong>", "")
                .replace("</strong>", "")
                .replace("<small>", "")
                .replace("</small>", "")
                .replace("<br />", "\n");
        result.estTime = substring(sauceSite, "<strong>Est Time: </strong>", "</span");
        return result;
    }

    private static String substring(String source, String prefix, String suffix) {
        int index = source.indexOf(prefix);
        if (index == -1) return "";
        source = source.substring(index + prefix.length());
        if (suffix != null) {
            index = source.indexOf(suffix);
            if (index == -1) return "";
            source = source.substring(0, index);
        }
        return source;
    }
}
