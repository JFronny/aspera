package io.gitlab.jfronny.aspera.util.rss;

import java.util.LinkedHashSet;
import java.util.Set;

public class RssInfo {
    public String title;
    public String link;
    public String description;
    public Set<Item> items = new LinkedHashSet<>();

    public static class Item {
        public String title;
        public String link;
        public String description;
        public String guid;
    }
}
