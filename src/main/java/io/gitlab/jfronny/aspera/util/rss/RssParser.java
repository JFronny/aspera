package io.gitlab.jfronny.aspera.util.rss;

import io.gitlab.jfronny.aspera.util.HttpUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.StringReader;
import java.util.LinkedHashSet;
import java.util.Set;

public class RssParser {
    public static RssInfo fetch(String url) throws ParserConfigurationException, IOException, SAXException {
        return parse(HttpUtils.get(url).sendString());
    }

    public static RssInfo parse(String text) throws ParserConfigurationException, IOException, SAXException {
        Document doc = DocumentBuilderFactory
                .newInstance()
                .newDocumentBuilder()
                .parse(new InputSource(new StringReader(text)));
        RssInfo info = new RssInfo();
        for (Node channel : getSub(doc.getElementsByTagName("channel"))) {
            for (Node node : getSub(channel.getChildNodes())) {
                switch (node.getNodeName()) {
                    case "title" -> info.title = node.getTextContent();
                    case "link" -> info.link = node.getTextContent();
                    case "description" -> info.description = node.getTextContent();
                    case "item" -> info.items.add(parseItem(node));
                }
            }
        }
        return info;
    }

    private static RssInfo.Item parseItem(Node itemNode) {
        RssInfo.Item item = new RssInfo.Item();
        for (Node node : getSub(itemNode.getChildNodes())) {
            switch (node.getNodeName()) {
                case "title" -> item.title = node.getTextContent();
                case "link" -> item.link = node.getTextContent();
                case "description" -> item.description = node.getTextContent();
                case "guid" -> item.guid = node.getTextContent();
            }
        }
        return item;
    }

    private static Set<Node> getSub(NodeList list) {
        Set<Node> nodes = new LinkedHashSet<>();
        for (int i = 0; i < list.getLength(); i++) {
            nodes.add(list.item(i));
        }
        return nodes;
    }
}
