package io.gitlab.jfronny.aspera.util.music;

import com.sedmelluq.discord.lavaplayer.track.AudioTrack;
import com.sedmelluq.discord.lavaplayer.track.AudioTrackInfo;
import net.dv8tion.jda.api.entities.User;

public record TrackWrapper(AudioTrack track, User author) {
    public TrackWrapper {
        track.setUserData(this);
    }

    public TrackWrapper makeClone() {
        return new TrackWrapper(track.makeClone(), author);
    }

    public AudioTrackInfo getInfo() {
        return track.getInfo();
    }
}
