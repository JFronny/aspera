package io.gitlab.jfronny.aspera.util.commands;

import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.utils.AttachmentOption;

import javax.annotation.Nonnull;
import java.io.InputStream;

public interface ReplyProvider {
    void reply(String text);
    void reply(Message message);
    FileReply reply(@Nonnull InputStream data, @Nonnull String fileName, @Nonnull AttachmentOption... options);
    boolean isMusicChannel();
}
