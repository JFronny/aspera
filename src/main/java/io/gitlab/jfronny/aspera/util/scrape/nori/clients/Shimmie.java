/*
 * This file is part of nori.
 * Copyright (c) 2014 Tomasz Jan Góralczyk <tomg@fastmail.uk>
 * License: ISC
 */

package io.gitlab.jfronny.aspera.util.scrape.nori.clients;

import io.gitlab.jfronny.aspera.util.HttpUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.net.URISyntaxException;
import java.util.Locale;

/**
 * Client for the Shimmie2 API.
 * Shimmie2 provides an extension that enables a Danbooru 1.x-based API.
 */
public class Shimmie extends DanbooruLegacy {

    //region Constructors

    public Shimmie(String endpoint) {
        super(endpoint);
    }
    //endregion

    //region Service detection

    /**
     * Checks if the given URL exposes a supported API endpoint.
     *
     * @param uri     URL to test.
     * @return Detected endpoint URL. null, if no supported endpoint URL was detected.
     */
    @Nullable
    public static String detectService(@NotNull String uri) throws URISyntaxException {
        final String endpointUrl = HttpUtils.appendPath(uri, "/api/danbooru/find_posts/index.xml");

        /*final String endpointUrl = Uri.withAppendedPath(uri, "/api/danbooru/find_posts/index.xml")
                .toString();

        try {
            final Response<DataEmitter> response = Ion.with(context)
                    .load(endpointUrl)
                    .setTimeout(timeout)
                    .userAgent(SearchClient.USER_AGENT)
                    .followRedirect(false)
                    .noCache()
                    .asDataEmitter()
                    .withResponse()
                    .get();

            // Close the connection.
            final DataEmitter dataEmitter = response.getResult();
            if (dataEmitter != null) dataEmitter.close();

            if (response.getHeaders().code() == 200) {
                return uri.toString();
            }
        } catch (InterruptedException | ExecutionException ignored) {
        }
        return null;*/
        if (HttpUtils.get(endpointUrl).sendProbe())
            return uri;
        return null;
    }
    //endregion

    //region Creating search URLs
    @Override
    protected String createSearchURL(String tags, int pid, int limit) {
        return String.format(Locale.US, "%s/api/danbooru/find_posts/index.xml?tags=%s&page=%d&limit=%d", apiEndpoint, tags, pid + 1, limit);
    }
    //endregion

    //region Parsing responses
    @Override
    protected String webUrlFromId(String id) {
        return String.format(Locale.US, "%s/post/view/%s", apiEndpoint, id);
    }
    //endregion
}
