package io.gitlab.jfronny.aspera.util.music;

import com.sedmelluq.discord.lavaplayer.player.AudioLoadResultHandler;
import com.sedmelluq.discord.lavaplayer.player.AudioPlayer;
import com.sedmelluq.discord.lavaplayer.player.AudioPlayerManager;
import com.sedmelluq.discord.lavaplayer.player.DefaultAudioPlayerManager;
import com.sedmelluq.discord.lavaplayer.player.event.AudioEventAdapter;
import com.sedmelluq.discord.lavaplayer.source.AudioSourceManagers;
import com.sedmelluq.discord.lavaplayer.tools.FriendlyException;
import com.sedmelluq.discord.lavaplayer.track.AudioPlaylist;
import com.sedmelluq.discord.lavaplayer.track.AudioTrack;
import com.sedmelluq.discord.lavaplayer.track.AudioTrackEndReason;
import io.gitlab.jfronny.aspera.Main;
import io.gitlab.jfronny.aspera.util.commands.ReplyProvider;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.entities.VoiceChannel;
import net.dv8tion.jda.api.managers.AudioManager;

import java.util.*;
import java.util.function.BiConsumer;

public class MusicStateHandler extends AudioEventAdapter {
    private static final AudioPlayerManager playerManager = new DefaultAudioPlayerManager();
    private static final Map<String, MusicStateHandler> audioPlayers = new HashMap<>();
    static {
        AudioSourceManagers.registerRemoteSources(playerManager);
    }

    public static MusicStateHandler getHandler(Guild guild) {
        if (guild == null) return null;
        if (!audioPlayers.containsKey(guild.getId()))
            audioPlayers.put(guild.getId(), new MusicStateHandler(guild));
        return audioPlayers.get(guild.getId());
    }

    private final AudioPlayer player;
    public List<TrackWrapper> tracks = new ArrayList<>();
    private final AudioManager audioManager;
    private ReplyProvider reply;
    private MusicStateHandler(Guild guild) {
        audioManager = guild.getAudioManager();
        player = playerManager.createPlayer();
        player.addListener(this);
    }

    public void connect(VoiceChannel channel) {
        audioManager.setSendingHandler(new AudioPlayerSendHandler(player));
        audioManager.openAudioConnection(channel);
    }

    public boolean isConnected() {
        return audioManager.isConnected();
    }

    public void disconnect() {
        tracks.clear();
        audioManager.closeAudioConnection();
        playNext(false);
    }

    public void search(String query, User author, BiConsumer<List<TrackWrapper>, String> success, ReplyProvider error) {
        boolean literal = true;
        if (!query.startsWith("http:") && !query.startsWith("https:")) {
            query = "ytmsearch:" + query;
            literal = false;
        }
        Main.LOGGER.info("Searching for entries matching: \"" + query + "\"");
        final boolean finalLiteral = literal;
        playerManager.loadItem(query, new AudioLoadResultHandler() {
            @Override
            public void trackLoaded(AudioTrack track) {
                success.accept(List.of(new TrackWrapper(track, author)), track.getInfo().title);
            }

            @Override
            public void playlistLoaded(AudioPlaylist playlist) {
                if (!finalLiteral)
                    trackLoaded(playlist.getTracks().get(0));
                else {
                    List<TrackWrapper> tw = new ArrayList<>();
                    for (AudioTrack track : playlist.getTracks())
                        tw.add(new TrackWrapper(track, author));
                    success.accept(tw, playlist.getName());
                }
            }

            @Override
            public void noMatches() {
                success.accept(new ArrayList<>(), null);
            }

            @Override
            public void loadFailed(FriendlyException exception) {
                Main.LOGGER.error("Could not load tracks", exception);
                error.reply("Something went wrong: " + exception);
            }
        });
    }

    public void queue(TrackWrapper track, ReplyProvider reply) {
        this.reply = reply;
        if (track == null)
            return;
        if (player.startTrack(track.track(), true)) {
            setPaused(false, reply);
        } else {
            if (tracks.contains(track) || player.getPlayingTrack() == track.track())
                track = track.makeClone();
            tracks.add(track);
        }
        refreshUI();
    }

    public String getDisplayName(TrackWrapper track) {
        return track.getInfo().title + " (" + track.getInfo().author + ") - @" + track.author().getName();
    }

    public void setPaused(boolean paused, ReplyProvider reply) {
        this.reply = reply;
        player.setPaused(paused);
        refreshUI();
    }

    public boolean isPaused() {
        return player.isPaused();
    }

    public void setVolume(float volume, ReplyProvider reply) {
        this.reply = reply;
        player.setVolume((int)(volume * 100));
    }

    @Override
    public void onPlayerPause(AudioPlayer player) {
        reply.reply("Paused");
    }

    @Override
    public void onPlayerResume(AudioPlayer player) {
        reply.reply("Resumed");
    }

    @Override
    public void onTrackEnd(AudioPlayer player, AudioTrack track, AudioTrackEndReason endReason) {
        if (endReason.mayStartNext) {
            playNext(endReason != AudioTrackEndReason.STOPPED);
        } else {
            refreshUI();
        }

        // endReason == FINISHED: A track finished or died by an exception (mayStartNext = true).
        // endReason == LOAD_FAILED: Loading of a track failed (mayStartNext = true).
        // endReason == STOPPED: The player was stopped.
        // endReason == REPLACED: Another track started playing while this had not finished
        // endReason == CLEANUP: Player hasn't been queried for a while, if you want you can put a
        //                       clone of this back to your queue
    }

    @Override
    public void onTrackException(AudioPlayer player, AudioTrack track, FriendlyException exception) {
        reply.reply("Track failed");
        Main.LOGGER.error("Track failed", exception);
    }

    @Override
    public void onTrackStuck(AudioPlayer player, AudioTrack track, long thresholdMs) {
        reply.reply("Track stuck");
        playNext(true);
    }

    public void playNext(boolean allowDC) {
        if (hasNext()) {
            TrackWrapper track = tracks.remove(0);
            player.playTrack(track.track());
            if (!reply.isMusicChannel())
                reply.reply("Playing `" + track.getInfo().title + "` by " + track.getInfo().author);
        } else if (allowDC) {
            disconnect();
        } else {
            player.stopTrack();
        }
        refreshUI();
    }

    public TrackWrapper getPlaying() {
        AudioTrack at = player.getPlayingTrack();
        if (at == null)
            return null;
        return at.getUserData(TrackWrapper.class);
    }

    public boolean hasNext() {
        return !tracks.isEmpty();
    }

    private boolean uiLocked = false;
    public void refreshUI() {
        if (!uiLocked)
            MusicChannelMessageHandler.refreshUI(audioManager.getGuild());
    }

    public void lockUI(Runnable ex) {
        try {
            uiLocked = true;
            ex.run();
        }
        finally {
            uiLocked = false;
        }
    }
}
