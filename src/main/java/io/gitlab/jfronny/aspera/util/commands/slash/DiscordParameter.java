package io.gitlab.jfronny.aspera.util.commands.slash;

import io.gitlab.jfronny.aspera.util.commands.Parameter;
import io.gitlab.jfronny.aspera.util.commands.UnknownValueSupplierKeyException;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.interactions.commands.Command;
import net.dv8tion.jda.api.interactions.commands.OptionMapping;
import net.dv8tion.jda.api.interactions.commands.OptionType;
import org.apache.http.util.TextUtils;

import java.io.InvalidClassException;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class DiscordParameter {
    private final String name;
    private final String description;
    private final boolean required;
    private final Field field;
    private final OptionType optionType;
    private final List<Command.Choice> enumValues = new ArrayList<>();
    private Map<String, ?> valueSupplier = null;

    public DiscordParameter(Field field) throws Exception {
        field.setAccessible(true);
        name = field.getName();
        Parameter p = field.getAnnotation(Parameter.class);
        description = p.description();
        required = p.required();
        this.field = field;
        Class<?> type = field.getType();
        if (type.isAssignableFrom(String.class))
            optionType = OptionType.STRING;
        else if (type.isAssignableFrom(Long.class))
            optionType = OptionType.INTEGER;
        else if (type.isAssignableFrom(Integer.class))
            optionType = OptionType.INTEGER;
        else if (type.isEnum()) {
            optionType = OptionType.INTEGER;
            Object[] os = type.getEnumConstants();
            for (int i = 0; i < os.length; i++) {
                enumValues.add(new Command.Choice(os[i].toString(), i));
            }
        } else if (type.isAssignableFrom(Boolean.class))
            optionType = OptionType.BOOLEAN;
        else if (type.isAssignableFrom(User.class))
            optionType = OptionType.USER;
        else if (type.isAssignableFrom(Member.class))
            optionType = OptionType.USER;
        else if (AbstractChannel.class.isAssignableFrom(type))
            optionType = OptionType.CHANNEL;
        else if (type.isAssignableFrom(Role.class))
            optionType = OptionType.ROLE;
        else if (type.isAssignableFrom(IMentionable.class))
            optionType = OptionType.MENTIONABLE;
        else if (!TextUtils.isEmpty(p.valueSupplier())) {
            Field valSup = field.getDeclaringClass().getField(p.valueSupplier());
            assert valSup.getGenericType() instanceof ParameterizedType;
            assert ((ParameterizedType)valSup.getGenericType()).getRawType().equals(Map.class);
            assert ((ParameterizedType)valSup.getGenericType()).getActualTypeArguments()[0].equals(String.class);
            assert ((ParameterizedType)valSup.getGenericType()).getActualTypeArguments()[1].equals(type);
            optionType = OptionType.STRING;
            valueSupplier = Map.copyOf((Map<String, ?>)valSup.get(null));
        }
        else {
            throw new Exception("Type is not valid for " + field.getDeclaringClass().getTypeName() + "." + field.getName());
        }
    }

    public Object getValue(OptionMapping om, Class<?> targetType) throws Exception {
        if (om == null) return null;
        Object value;
        switch (optionType) {
            case STRING -> {
                if (valueSupplier == null)
                    value = om.getAsString();
                else {
                    String v = om.getAsString();
                    if (!valueSupplier.containsKey(v))
                        throw new UnknownValueSupplierKeyException(name);
                    value = valueSupplier.get(v);
                }
            }
            case INTEGER -> {
                if (targetType.isEnum())
                    value = targetType.getEnumConstants()[(int) om.getAsLong()];
                else if (targetType.isAssignableFrom(Long.class))
                    value = Long.valueOf(om.getAsLong());
                else
                    value = Integer.valueOf((int)om.getAsLong());
            }
            case BOOLEAN -> value = Boolean.valueOf(om.getAsBoolean());
            case USER -> {
                if (targetType.isAssignableFrom(User.class))
                    value = om.getAsUser();
                else
                    value = om.getAsMember();
            }
            case CHANNEL -> {
                if (MessageChannel.class.isAssignableFrom(targetType))
                    value = om.getAsMessageChannel();
                else if (GuildChannel.class.isAssignableFrom(targetType))
                    value = om.getAsGuildChannel();
                else throw new InvalidClassException("Target type not valid for channel");
            }
            case ROLE -> value = om.getAsRole();
            case MENTIONABLE -> value = om.getAsMentionable();
            default -> throw new InvalidClassException("Target type not valid for channel");
        };
        return value;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public boolean isRequired() {
        return required;
    }

    public Field getField() {
        return field;
    }

    public OptionType getOptionType() {
        return optionType;
    }

    public List<Command.Choice> getEnumValues() {
        return enumValues;
    }

    public Map<String, ?> getValueSupplier() {
        return valueSupplier;
    }
}
