/*
 * This file is part of nori.
 * Copyright (c) 2014 Tomasz Jan Góralczyk <tomg@fastmail.uk>
 * License: ISC
 */

package io.gitlab.jfronny.aspera.util.scrape.nori.clients;

import io.gitlab.jfronny.aspera.util.scrape.nori.SearchResult;

import java.io.IOException;
import java.net.URISyntaxException;

/**
 * Interface for a client consuming a Danbooru style API.
 */
public interface SearchClient {
    //region Search methods

    /**
     * Fetch first page of results containing images with the given set of tags.
     *
     * @param tags Search query. A space-separated list of tags.
     * @return A {@link SearchResult} containing a set of Images.
     * @throws IOException Network error.
     */
    SearchResult search(String tags) throws IOException, URISyntaxException;

    /**
     * Search for images with the given set of tags.
     *
     * @param tags Search query. A space-separated list of tags.
     * @param pid  Page number. (zero-indexed)
     * @return A {@link SearchResult} containing a set of Images.
     * @throws IOException Network error.
     */
    SearchResult search(String tags, int pid) throws IOException, URISyntaxException;
    //endregion

    //region Default query

    /**
     * Get a SafeSearch default query to search for when an app is launched.
     *
     * @return Safe-for-work query to search for when an app is launched.
     */
    String getDefaultQuery();
}
