package io.gitlab.jfronny.aspera.util.commands;

public enum CommandTag {
    AuthorOnly,
    GuildOnly,
    AdminOnly,
    MusicOnly,
    NsfwOnly
}
