package io.gitlab.jfronny.aspera.util.music;

import io.gitlab.jfronny.aspera.Main;
import io.gitlab.jfronny.aspera.util.MessageHandler;
import io.gitlab.jfronny.aspera.util.commands.ReplyProvider;
import io.gitlab.jfronny.aspera.commands.music.QueueCommand;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.GuildVoiceState;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.events.interaction.ButtonClickEvent;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.interactions.components.ActionRow;
import net.dv8tion.jda.api.interactions.components.Button;
import net.dv8tion.jda.api.interactions.components.Component;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;

public class MusicChannelMessageHandler extends MessageHandler {
    public MusicChannelMessageHandler() {
        super("musicChannel");
    }

    @Override
    public void handleMessage(@NotNull GuildMessageReceivedEvent event) {
        if (event.isWebhookMessage() || event.getAuthor().isBot()) return;
        try {
            TextChannel channel = event.getChannel();
            if (Main.getDatabase().getGuild(event.getGuild()).getMusicChannels().contains(channel.getId())) {
                MusicStateHandler music = MusicStateHandler.getHandler(event.getGuild());
                ReplyProvider reply = getReply(event.getChannel(), event.getGuild());
                if (!music.isConnected()) {
                    GuildVoiceState state = event.getMember().getVoiceState();
                    if (state != null && state.inVoiceChannel()) {
                        music.connect(state.getChannel());
                    }
                    else {
                        reply.reply("Hey, you can only use this in a voice channel");
                        return;
                    }
                }
                music.search(event.getMessage().getContentDisplay(), event.getAuthor(), (songs, name) -> {
                    for (TrackWrapper song : songs) music.queue(song, reply);
                }, reply);
                event.getMessage().delete().queue(s -> refreshUI(event.getGuild()));
            }
        } catch (Exception e) {
            Main.LOGGER.error("Could not check whether a message was sent in a music channel", e);
        }
    }

    @Override
    public void handleButtonClick(ButtonClickEvent event, String id) throws Exception {
        MusicStateHandler handler = MusicStateHandler.getHandler(event.getGuild());
        handler.lockUI(() -> {
            switch (id) {
                case "pause" -> handler.setPaused(!handler.isPaused(), getReply(event.getTextChannel(), event.getGuild()));
                case "dc" -> handler.disconnect();
                case "skip" -> handler.playNext(true);
                case "shuffle" -> Collections.shuffle(handler.tracks);
                case "clear" -> handler.tracks.clear();
            }
            event.editMessage(genMessage(handler)).queue();
        });
    }

    private static Message genMessage(MusicStateHandler music) {
        Collection<Component> components = new LinkedHashSet<>();
        components.add(Button.danger("musicChannel.pause", "⏯️"));
        components.add(Button.danger("musicChannel.dc", "⏹"));
        components.add(Button.danger("musicChannel.skip", "⏭️️"));
        components.add(Button.danger("musicChannel.shuffle", "\uD83D\uDD00️️"));
        components.add(Button.danger("musicChannel.clear", "❌️️"));
        return new MessageBuilder(QueueCommand.getPageText(music, 0))
                .setActionRows(ActionRow.of(components)).build();
    }

    public static synchronized void refreshUI(Guild guild) {
        try {
            MusicStateHandler music = MusicStateHandler.getHandler(guild);
            for (String musicChannel : Main.getDatabase().getGuild(guild).getMusicChannels()) {
                TextChannel channel = guild.getTextChannelById(musicChannel);
                if (channel == null) return;
                channel.getHistory().retrievePast(50).queue(messages -> {
                    AtomicReference<Message> found = new AtomicReference<>(null);
                    for (Message message : new LinkedList<>(messages)) {
                        if (message.getAuthor().getIdLong() == Main.getBot().getAccount().getIdLong() && message.isPinned()) {
                            messages.remove(message);
                            if (found.get() == null) found.set(message);
                            else found.get().delete().queue();
                        }
                    }
                    if (found.get() != null)
                        found.get().editMessage(genMessage(music)).queue();
                    if (messages.size() < 2)
                        for (Message message : messages)
                            message.delete().queue();
                    else
                        channel.deleteMessages(messages).queue();

                    if (found.get() == null)
                        channel.sendMessage(genMessage(music)).queue(message -> message.pin().queue());
                });
            }
        } catch (IOException e) {
            Main.LOGGER.error("Could not refresh Music UI in " + guild.getName(), e);
        }
    }
}
