/*
 * This file is part of nori.
 * Copyright (c) 2014 Tomasz Jan Góralczyk <tomg@fastmail.uk>
 * License: ISC
 */

package io.gitlab.jfronny.aspera.util.scrape.nori;

import io.gitlab.jfronny.aspera.util.scrape.nori.clients.*;
import org.apache.http.client.utils.URIBuilder;

import java.net.URI;

/**
 * Service that detects the {@link SearchClient} API type for given URL.
 */
public class ServiceTypeResolver {
    //region Constants (Broadcast result IDs)
    /**
     * Result code returned when the service type was detected successfully.
     */
    public static final int RESULT_OK = 0x00;
    /**
     * Result code returned when the URL given was not a valid URL.
     */
    public static final int RESULT_FAIL_INVALID_URL = 0x02;
    /**
     * Result code returned when no valid API was found at given URL.
     */
    public static final int RESULT_FAIL_NO_API = 0x03;
    //endregion

    //region Constants (Service detection settings)
    /**
     * Uri schemes to use when detecting services.
     */
    public static final String[] URI_SCHEMES = {"https", "http"};
    // so we wait longer for SSL checks to complete.
    //endregion

    //region IntentService methods (onHandleIntent)
    public static DetectionResult discoverClient(URI uri) throws Exception {
        if (uri.getHost() == null || uri.getScheme() == null) {
            // The URL supplied is invalid.
            throw new Exception("Invalid URL: no host or scheme");
        }

        // Detected API endpoint.
        String apiEndpoint;

        // Check for flickr and E621 (services with hardcoded URLs).
        apiEndpoint = Flickr.detectService(uri);
        if (apiEndpoint != null) {
            return new DetectionResult(apiEndpoint, new Flickr(apiEndpoint));
        }
        apiEndpoint = FlickrUser.detectService(uri);
        if (apiEndpoint != null) {
            return new DetectionResult(apiEndpoint, new FlickrUser(apiEndpoint));
        }

        // Iterate over supported URI schemes for given URL.
        for (String uriScheme : URI_SCHEMES) {
            final URI baseUri = new URIBuilder().setScheme(uriScheme).setHost(uri.getHost())
                    .setPath(uri.getPath()).build();

            apiEndpoint = Danbooru.detectService(baseUri.toString());
            if (apiEndpoint != null) {
                return new DetectionResult(apiEndpoint, new Danbooru(apiEndpoint));
            }

            apiEndpoint = DanbooruLegacy.detectService(baseUri.toString());
            if (apiEndpoint != null) {
                return new DetectionResult(apiEndpoint, new DanbooruLegacy(apiEndpoint));
            }

            apiEndpoint = Gelbooru.detectService(baseUri.toString());
            if (apiEndpoint != null) {
                return new DetectionResult(apiEndpoint, new Gelbooru(apiEndpoint));
            }

            apiEndpoint = Shimmie.detectService(baseUri.toString());
            if (apiEndpoint != null) {
                return new DetectionResult(apiEndpoint, new Shimmie(apiEndpoint));
            }
        }

        throw new Exception("Could not find a compatible service for " + uri);
    }
    //endregion

    public static record DetectionResult(String endpointUrl, SearchClient client) {
    }
}
