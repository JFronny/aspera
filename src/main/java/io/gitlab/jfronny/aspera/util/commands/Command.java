package io.gitlab.jfronny.aspera.util.commands;

import io.gitlab.jfronny.aspera.Main;
import io.gitlab.jfronny.aspera.util.commands.slash.DiscordParameter;
import io.gitlab.jfronny.aspera.util.music.MusicStateHandler;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.events.interaction.ButtonClickEvent;
import net.dv8tion.jda.api.interactions.commands.build.CommandData;
import net.dv8tion.jda.api.interactions.commands.build.OptionData;
import net.dv8tion.jda.api.interactions.commands.build.SubcommandData;
import net.dv8tion.jda.api.requests.restaction.MessageAction;
import net.dv8tion.jda.api.utils.AttachmentOption;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.util.*;

public abstract class Command implements ReplyProvider {
    private final String name;
    private final String description;
    private final List<CommandTag> tags;
    private final Set<DiscordParameter> options = new LinkedHashSet<>();

    public Command(String name, String description, CommandTag... tags) throws Exception {
        this.name = name;
        this.description = description;
        this.tags = new ArrayList<>(Arrays.asList(tags));
        Field[] fields = this.getClass().getDeclaredFields();
        /*for (Field field : fields) {
            if (field.isAnnotationPresent(ValueSupplier.class)) {
                ParameterizedType pType = (ParameterizedType)field.getGenericType();
                assert pType.getRawType().equals(Map.class);
                assert pType.getActualTypeArguments()[0].equals(String.class);
                assert Modifier.isStatic(field.getModifiers());
                valueSuppliers.put(field.getAnnotation(ValueSupplier.class).affectedField(), field);
            }
        }*/
        for (Field field : fields) {
            if (field.isAnnotationPresent(Parameter.class)) {
                options.add(new DiscordParameter(field));
            }
        }
    }

    public boolean hasTag(CommandTag tag) {
        return tags.contains(tag);
    }
    public Command addTag(CommandTag tag) {
        tags.add(tag);
        return this;
    }

    public String getName() {
        return name;
    }
    public String getDescription() {
        return description;
    }

    public Set<DiscordParameter> getOptions() {
        return options;
    }

    protected Guild guild;
    protected MessageChannel channel;
    protected User user;
    protected MusicStateHandler music;
    protected CommandExecutionArgs args;

    public abstract void execute() throws Exception;

    public void execute(CommandExecutionArgs args) {
        this.args = args;
        if (hasTag(CommandTag.GuildOnly) && !args.isGuild()) {
            reply("This command can only be used in guilds");
            return;
        }
        if (hasTag(CommandTag.AdminOnly) && !args.callerHasPermission(Permission.ADMINISTRATOR)) {
            reply("You need to have admin privileges to use this");
            return;
        }
        if (Main.getConfig().botOwner != null && hasTag(CommandTag.AuthorOnly) && !Main.getConfig().botOwner.equals(args.getUser().getId())) {
            reply("Only the owner of this instance of the bot may use this command");
            return;
        }
        if (hasTag(CommandTag.NsfwOnly) && !args.isNsfwAllowed()) {
            reply("This command may only be used in NSFW channels");
            return;
        }
        CommandExecutionArgs.OptionSupplier os = args.getOptionSupplier();
        os.reset();
        for (DiscordParameter option : options) {
            try {
                Field f = option.getField();
                f.set(this, os.nextValue(new CommandExecutionArgs.OptionSupplierParameter<>(option, f.getType())));
            } catch (Exception e) {
                Main.LOGGER.error("Could not set command option", e);
            }
        }
        try {
            guild = args.getGuild();
            channel = args.getChannel();
            user = args.getUser();
            music = MusicStateHandler.getHandler(guild);
            if (music == null && hasTag(CommandTag.MusicOnly)) {
                reply("This command must either be used in a guild or while connected to a VC");
                return;
            }
            execute();
        }
        catch (Exception e) {
            Main.LOGGER.error("Could not execute command: " + getName(), e);
            reply("Something went wrong on our side. Please inform this instances admin");
        }
    }

    public void handleButtonClick(ButtonClickEvent event, String id) throws Exception {

    }

    @Override
    public void reply(String message) {
        args.getReply().reply(message);
    }

    @Override
    public void reply(Message message) {
        args.getReply().reply(message);
    }

    @Override
    public FileReply reply(@NotNull InputStream data, @NotNull String fileName, @NotNull AttachmentOption... options) {
        if (data == null || fileName == null)
            throw new NullPointerException();
        return args.getReply().reply(data, fileName, options);
    }

    @Override
    public boolean isMusicChannel() {
        try {
            return Main.getDatabase().getGuild(guild).getMusicChannels().contains(channel.getId());
        } catch (IOException e) {
            Main.LOGGER.error("Failed to check music channel status", e);
            return false;
        }
    }

    public CommandData getData() throws Exception {
        CommandData data = new CommandData(getName(), getDescription());
        if (this instanceof CommandGroup cg) {
            for (Command value : cg.getSubcommands().values()) {
                if (value instanceof CommandGroup)
                    throw new Exception("Stacked command groups are not currently supported");
                if (Main.getConfig().botOwner == null && value.hasTag(CommandTag.AuthorOnly)) {
                    Main.LOGGER.info("Skipping " + getName() + "." + value.getName() + ": botOwner not specified");
                }
                data.addSubcommands(cg.getSubcommand(value.getName()).getSubData());
            }
        }
        Set<OptionData> nOp = new LinkedHashSet<>();
        for (DiscordParameter option : options) {
            OptionData od = new OptionData(option.getOptionType(), option.getName(), option.getDescription(), option.isRequired());
            if (!option.getEnumValues().isEmpty()) {
                od.addChoices(option.getEnumValues());
            }
            else if (option.getValueSupplier() != null) {
                for (String s : option.getValueSupplier().keySet()) {
                    od.addChoice(s, s);
                }
            }
            nOp.add(od);
        }
        if (!nOp.isEmpty())
            data.addOptions(nOp);
        return data;
    }

    public SubcommandData getSubData() {
        SubcommandData data = new SubcommandData(getName(), getDescription());
        for (DiscordParameter option : options) {
            data.addOption(option.getOptionType(), option.getName(), option.getDescription(), option.isRequired());
        }
        return data;
    }
}
