/*
 * This file is part of nori.
 * Copyright (c) 2014-2016 Tomasz Jan Góralczyk <tomg@fastmail.uk>
 * License: GNU GPLv2
 */

package io.gitlab.jfronny.aspera.util.scrape.nori.clients;

import edu.umd.cs.findbugs.annotations.NonNull;
import io.gitlab.jfronny.aspera.util.HttpUtils;
import io.gitlab.jfronny.aspera.util.scrape.nori.Image;
import io.gitlab.jfronny.aspera.util.scrape.nori.SearchResult;
import io.gitlab.jfronny.aspera.util.scrape.nori.Tag;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.util.TextUtils;
import org.jetbrains.annotations.Nullable;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.StringReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Search client for the Flickr API.
 */
public class Flickr implements SearchClient {

    //region Constants
    /**
     * Default API endpoint.
     */
    public static final URI FLICKR_API_ENDPOINT = URI.create("https://api.flickr.com/services/rest");
    /**
     * Number of images to fetch per page.
     */
    protected static final int DEFAULT_LIMIT = 100;
    /**
     * Public API key used to access Flickr services.
     */
    protected static final String FLICKR_API_KEY = "6b74179518fc00c8bef70b230c7ee880";
    //endregion

    //region Instance fields
    /**
     * API Endpoint.
     */
    protected final URI apiEndpoint;
    //endregion

    //region Constructors

    /**
     * Create a new Flickr API client.
     *
     * @param apiEndpoint API endpoint. (i.e. https://api.flickr.com/services)
     */
    public Flickr(String apiEndpoint) {
        this.apiEndpoint = apiEndpoint != null ? URI.create(apiEndpoint) : FLICKR_API_ENDPOINT;
    }
    //endregion

    //region Service detection

    /**
     * Checks if the given URL exposes a supported API endpoint.
     *
     * @param uri URL to test.
     * @return Detected endpoint URL. null, if no supported endpoint URL was detected.
     */
    @Nullable
    public static String detectService(@NonNull URI uri) {
        final String host = uri.getHost();

        // Check for hardcoded URL.
        if ("api.flickr.com".equals(host))
            return FLICKR_API_ENDPOINT.toString();
        return null;
    }
    //endregion

    //region SearchClient methods

    /**
     * Fetch first page of results containing images with the given set of tags.
     *
     * @param tags Search query. A space-separated list of tags.
     * @return A {@link SearchResult} containing a set of Images.
     * @throws IOException Network error.
     */
    @Override
    public SearchResult search(String tags) throws IOException, URISyntaxException {
        // Return results for page 0.
        return search(tags, 0);
    }

    /**
     * Search for images with the given set of tags.
     *
     * @param tags Search query. A space-separated list of tags.
     * @param pid  Page number. (zero-indexed)
     * @return A {@link SearchResult} containing a set of Images.
     * @throws IOException Network error.
     */
    @Override
    public SearchResult search(String tags, int pid) throws IOException, URISyntaxException {
        /*try {
            return Ion.with(this.context)
                    .load(createSearchURL(tags, pid))
                    .userAgent(USER_AGENT)
                    .as(new SearchResultParser(tags, pid))
                    .get();
        } catch (InterruptedException | ExecutionException e) {
            // Normalise exception to IOException, so method signatures are not tied to a single HTTP
            // library.
            throw new IOException(e);
        }*/
        return parseXMLResponse(HttpUtils.get(createSearchURL(tags, pid)).sendString(), tags, pid);
    }

    /**
     * Get a SafeSearch default query to search for when an app is launched.
     *
     * @return Safe-for-work query to search for when an app is launched.
     */
    @Override
    public String getDefaultQuery() {
        return "";
    }

    //region Creating search URLs

    /**
     * Generate request URL to the search API endpoint.
     *
     * @param tags Space-separated tags.
     * @param pid  Page number (0-indexed).
     * @return URL to search results API.
     */
    protected String createSearchURL(String tags, int pid) throws URISyntaxException {
        return new URIBuilder(apiEndpoint)
                .setParameter("api_key", FLICKR_API_KEY)
                .setParameter("method", !TextUtils.isEmpty(tags) ? "flickr.photos.search" : "flickr.interestingness.getList")
                .setParameter("text", tags != null ? tags : "")
                .setParameter("per_page", Integer.toString(DEFAULT_LIMIT, 10))
                .setParameter("extras", "date_upload,owner_name,media,tags,path_alias,icon_server,o_dims,path_alias,original_format,url_q,url_m,url_l,url_o")
                .setParameter("page", Integer.toString(pid + 1, 10))
                .build()
                .toString();
    }
    //endregion

    //region Parsing responses

    /**
     * Parse an XML response returned by the API.
     *
     * @param body   HTTP Response body.
     * @param tags   Tags used to retrieve the response.
     * @param offset Current paging offset.
     * @return A {@link SearchResult} parsed from given XML.
     */
    protected SearchResult parseXMLResponse(String body, String tags, int offset) throws IOException {
        if (body == null)
            throw new IOException("Couldn't read");
        final List<Image> imageList = new ArrayList<>(DEFAULT_LIMIT);

        try {
            Document doc = DocumentBuilderFactory
                    .newInstance()
                    .newDocumentBuilder()
                    .parse(new InputSource(new StringReader(body)));

            NodeList nodeList = doc.getElementsByTagName("photo");

            for (int i = 0; i < nodeList.getLength(); i++) {
                Node node = nodeList.item(i);

                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element element = (Element) node;

                    final Image image = new Image();
                    image.searchPage = offset;
                    image.searchPagePosition = i;

                    String urlQ = element.getAttribute("url_q");
                    String urlM = element.getAttribute("url_m");
                    String urlL = element.getAttribute("url_l");
                    String urlO = element.getAttribute("url_o");

                    // Set file url.
                    if (!TextUtils.isEmpty(urlO)) {
                        image.fileUrl = urlO;
                        image.width = Integer.parseInt(element.getAttribute("width_o"));
                        image.height = Integer.parseInt(element.getAttribute("height_o"));
                    } else if (!TextUtils.isEmpty(urlL)) {
                        image.fileUrl = urlL;
                        image.width = Integer.parseInt(element.getAttribute("width_l"));
                        image.height = Integer.parseInt(element.getAttribute("height_l"));
                    } else if (!TextUtils.isEmpty(urlM)) {
                        image.fileUrl = urlM;
                        image.width = Integer.parseInt(element.getAttribute("width_m"));
                        image.height = Integer.parseInt(element.getAttribute("height_m"));
                    }

                    // Set sample url.
                    if (!TextUtils.isEmpty(urlL)) {
                        image.sampleUrl = urlL;
                        image.sampleWidth = Integer.parseInt(element.getAttribute("width_l"));
                        image.sampleHeight = Integer.parseInt(element.getAttribute("height_l"));
                    } else if (!TextUtils.isEmpty(urlM)) {
                        image.sampleUrl = urlM;
                        image.sampleWidth = Integer.parseInt(element.getAttribute("width_m"));
                        image.sampleHeight = Integer.parseInt(element.getAttribute("height_m"));
                    } else if (!TextUtils.isEmpty(urlQ)) {
                        image.sampleUrl = urlQ;
                        image.sampleWidth = Integer.parseInt(element.getAttribute("width_q"));
                        image.sampleHeight = Integer.parseInt(element.getAttribute("height_q"));
                    }

                    // Set preview url.
                    if (!TextUtils.isEmpty(urlQ)) {
                        image.previewUrl = urlQ;
                        image.previewWidth = Integer.parseInt(element.getAttribute("width_q"));
                        image.previewHeight = Integer.parseInt(element.getAttribute("height_q"));
                    }

                    image.tags = Tag.arrayFromString(element.getAttribute("tags"));
                    image.id = element.getAttribute("id");
                    image.webUrl = webUrlFromId(element.getAttribute("owner"), element.getAttribute("id"));
                    image.parentId = null;
                    image.safeSearchRating = Image.SafeSearchRating.S;
                    image.score = 0;
                    image.md5 = "2d57d21f35e060a4c5e81c03aea3efa8"; // not implemented
                    image.createdAt = new Date(Long.parseLong(element.getAttribute("dateupload"), 10) * 1000);

                    imageList.add(image);
                }
            }

        } catch (SAXException | ParserConfigurationException e) {
            throw new IOException(e);
        }

        return new SearchResult(imageList.toArray(new Image[imageList.size()]), Tag.arrayFromString(tags), offset);
    }


    /**
     * Create Flickr web url for given user and photo id.
     */
    protected String webUrlFromId(String userId, String photoId) {
        return "https://www.flickr.com/photos/" + userId + "/" + photoId;
    }
    //endregion
}
