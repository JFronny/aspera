package io.gitlab.jfronny.aspera.util.commands.slash;

import io.gitlab.jfronny.aspera.Main;
import io.gitlab.jfronny.aspera.util.commands.Command;
import io.gitlab.jfronny.aspera.util.commands.CommandGroup;
import io.gitlab.jfronny.aspera.util.commands.CommandTag;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.interactions.commands.build.CommandData;
import net.dv8tion.jda.api.requests.restaction.CommandListUpdateAction;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class CommandManager {
    private final JDA API;
    private final String botTestServer;
    private InitState state = InitState.Constructed;
    private CommandListUpdateAction initCommandAction;
    private final Map<String, Command> globalCommands = new LinkedHashMap<>();
    private final Map<String, Map<String, Command>> guildCommands = new LinkedHashMap<>();
    public CommandManager(JDA api, String botTestServer) {
        this.API = api;
        this.botTestServer = botTestServer;
    }

    public void initialize() {
        if (botTestServer == null) {
            Main.LOGGER.info("Using prod command update system. Expect delays when adding new commands");
            initCommandAction = API.updateCommands();
        }
        else {
            Main.LOGGER.info("Using debug command update system. Don't use this in prod!");
            Guild g = API.getGuildById(botTestServer);
            if (g != null) {
                Main.LOGGER.info("Debug server: " + g.getName());
                initCommandAction = g.updateCommands();
                API.updateCommands().queue();
            }
            else {
                Main.LOGGER.info("Error: Specified debug server could not be found. Using prod system.");
                initCommandAction = API.updateCommands();
            }
        }
        List<Guild> guilds = API.getGuilds();
        Main.LOGGER.info("Connected to %d guild%s".formatted(guilds.size(), guilds.size() == 1 ? "" : "s"));
        for (Guild guild : API.getGuilds()) {
            guild.updateCommands().queue(); //TODO restore custom commands
            Main.LOGGER.info("- " + guild.getName() + " (" + guild.getMemberCount() + " members)");
        }
        state = InitState.Initializing;
    }

    public void finalizeInit() throws Exception {
        if (state != InitState.Initializing)
            throw new Exception("DiscordBackend is not initializing");
        initCommandAction.queue();
        state = InitState.Initialized;
    }

    public void registerCommand(Command command, @Nullable Guild guild) throws Exception {
        if (Main.getConfig().botOwner == null && command.hasTag(CommandTag.AuthorOnly)) {
            Main.LOGGER.info("Skipping " + command.getName() + ": botOwner not specified");
        }
        if (state != InitState.Initializing && guild == null)
            throw new Exception("Global commands can only be registered during init");
        if (globalCommands.containsKey(command.getName()) || guildCommands.containsKey(command.getName()))
            throw new Exception("A command using this name is already registered. Skipping");
        CommandData data = command.getData();
        if (guild == null) {
            initCommandAction = initCommandAction.addCommands(data);
            globalCommands.put(data.getName(), command);
        }
        else {
            guild.upsertCommand(data).queue();
            if (!guildCommands.containsKey(guild.getId()))
                guildCommands.put(guild.getId(), new LinkedHashMap<>());
            guildCommands.get(guild.getId()).put(data.getName(), command);
        }
    }

    public void removeCommand(Command command, @NotNull Guild guild) throws Exception {
        if (!guildCommands.containsKey(guild.getId()) || !guildCommands.get(guild.getId()).containsKey(command.getName()))
            throw new Exception("This command is not registered in the provided guild");
        guild.deleteCommandById(command.getName()).queue();
    }

    public Command getCommand(String name, @Nullable Guild guild) throws Exception {
        if (name.contains(" ")) {
            String[] cmd = name.split(" ", 2);
            Command cmdE = getCommand(cmd[0], guild);
            if (!(cmdE instanceof CommandGroup cg))
                throw new Exception("Tried to access subcommand of non-group");
            return cg.getSubcommand(cmd[1]);
        }
        if (globalCommands.containsKey(name))
            return globalCommands.get(name);
        if (guild == null)
            throw new Exception("Could not find command and no guild was specified");
        if (guildCommands.containsKey(guild.getId())) {
            Map<String, Command> c = guildCommands.get(guild.getId());
            if (c.containsKey(name))
                return c.get(name);
        }
        throw new Exception("Could not find command");
    }

    public enum InitState {
        Constructed, Initializing, Initialized
    }
}
