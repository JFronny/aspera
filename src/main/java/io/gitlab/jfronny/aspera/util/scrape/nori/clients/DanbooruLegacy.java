/*
 * This file is part of nori.
 * Copyright (c) 2014 Tomasz Jan Góralczyk <tomg@fastmail.uk>
 * License: ISC
 */

package io.gitlab.jfronny.aspera.util.scrape.nori.clients;

import edu.umd.cs.findbugs.annotations.NonNull;
import io.gitlab.jfronny.aspera.util.HttpUtils;
import io.gitlab.jfronny.aspera.util.scrape.nori.Image;
import io.gitlab.jfronny.aspera.util.scrape.nori.SearchResult;
import io.gitlab.jfronny.aspera.util.scrape.nori.Tag;
import org.apache.http.client.utils.DateUtils;
import org.apache.http.util.TextUtils;
import org.jetbrains.annotations.Nullable;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.function.Predicate;
import java.util.regex.Pattern;

/**
 * Client for the Danbooru 1.x API.
 */
public class DanbooruLegacy implements SearchClient {
    //region Constants
    /**
     * Number of images per search results page.
     * Best to use a large value to minimize number of unique HTTP requests.
     */
    private static final int DEFAULT_LIMIT = 100;
    //endregion

    //region Service configuration instance fields
    /**
     * URL to the HTTP API Endpoint - the server implementing the API.
     */
    protected final String apiEndpoint;
    //endregion

    //region Constructors

    /**
     * Create a new Danbooru 1.x client without authentication.
     *
     * @param endpoint URL to the HTTP API Endpoint - the server implementing the API.
     */
    public DanbooruLegacy(String endpoint) {
        this.apiEndpoint = endpoint;
    }
    //endregion

    //region Service detection

    /**
     * Checks if the given URL exposes a supported API endpoint.
     *
     * @param uri     URL to test.
     * @return Detected endpoint URL. null, if no supported endpoint URL was detected.
     */
    @Nullable
    public static String detectService(@NonNull String uri) throws URISyntaxException {
        final String endpointUrl = HttpUtils.appendPath(uri, "/post/index.xml");

        /*final String endpointUrl = Uri.withAppendedPath(uri, "/post/index.xml").toString();

        try {
            final Response<DataEmitter> response = Ion.with(context)
                    .load(endpointUrl)
                    .setTimeout(timeout)
                    .userAgent(SearchClient.USER_AGENT)
                    .followRedirect(false)
                    .noCache()
                    .asDataEmitter()
                    .withResponse()
                    .get();

            // Close the connection.
            final DataEmitter dataEmitter = response.getResult();
            if (dataEmitter != null) dataEmitter.close();

            if (response.getHeaders().code() == 200) {
                return uri.toString();
            }
        } catch (InterruptedException | ExecutionException ignored) {
        }*/
        if (HttpUtils.get(endpointUrl).sendProbe())
            return uri;
        return null;
    }
    //endregion

    //region SearchClient methods
    @Override
    public SearchResult search(String tags) throws IOException {
        // Return results for page 0.
        return search(tags, 0);
    }

    @Override
    public SearchResult search(String tags, int pid) throws IOException {
        return parseXMLResponse(HttpUtils.get(createSearchURL(tags, pid, DEFAULT_LIMIT)).sendString(), tags, pid);
    }

    @Override
    public String getDefaultQuery() {
        // Show all safe-for-work images by default.
        return "";
    }
    //endregion

    //region Creating Search URLs

    /**
     * Generate request URL to the search API endpoint.
     *
     * @param tags  Space-separated tags.
     * @param pid   Page number (0-indexed).
     * @param limit Images to fetch per page.
     * @return URL to search results API.
     */
    protected String createSearchURL(String tags, int pid, int limit) {
        return String.format(Locale.US, apiEndpoint + "/post/index.xml?tags=%s&limit=%d&page=%d", URLEncoder.encode(tags, StandardCharsets.UTF_8), limit, pid + 1);
    }
    //endregion

    //region Parsing responses

    /**
     * Parse an XML response returned by the API.
     *
     * @param body   HTTP Response body.
     * @param tags   Tags used to retrieve the response.
     * @param offset Current paging offset.
     * @return A {@link SearchResult} parsed from given XML.
     */
    protected SearchResult parseXMLResponse(String body, String tags, int offset) throws IOException {
        if (body == null)
            throw new IOException("Couldn't read");
        // Create variables to hold the values as XML is being parsed.
        final List<Image> imageList = new ArrayList<>(DEFAULT_LIMIT);
        int position = 0;

        try {
            // Create an XML parser factory and disable namespace awareness for security reasons.
            // See: (http://lists.w3.org/Archives/Public/public-xmlsec/2009Dec/att-0000/sws5-jensen.pdf).
            final XmlPullParserFactory xmlParserFactory = XmlPullParserFactory.newInstance();
            xmlParserFactory.setNamespaceAware(false);

            // Create a new XML parser and feed HTTP response data into it.
            final XmlPullParser xpp = xmlParserFactory.newPullParser();
            xpp.setInput(new StringReader(body));

            Image image = null;
            String previousTag = null;
            // Iterate over each XML element and handle pull parser "events".
            while (xpp.getEventType() != XmlPullParser.END_DOCUMENT) {
                String n = xpp.getName();
                if (xpp.getEventType() == XmlPullParser.START_TAG) {
                    if ("post".equals(n)) {
                        // <post> tags contain metadata for each image.
                        image = new Image();
                        image.searchPage = offset;
                        image.searchPagePosition = position;

                        // Extract image metadata from XML attributes.
                        for (int i = 0; i < xpp.getAttributeCount(); i++) {
                            // Get name and value of current XML attribute.
                            final String name = xpp.getAttributeName(i);
                            final String value = xpp.getAttributeValue(i);

                            // Set the appropriate value for each tag name.
                            mutateImage(image, name, value);
                        }

                        // Add Image to search result.
                        imageList.add(image);
                        position++;
                    } else if (!"posts".equals(n) && image != null) {
                        //mutateImage(image, n, xpp.getText());
                    }
                }
                else if (xpp.getEventType() == XmlPullParser.TEXT) {
                    mutateImage(image, previousTag, xpp.getText());
                }
                else if (xpp.getEventType() == XmlPullParser.END_TAG && "post".equals(xpp.getName())) {
                    image = null;
                }
                previousTag = n;
                // Get next XMLPullParser event.
                xpp.next();
            }
        } catch (XmlPullParserException e) {
            // Convert into IOException.
            // Needed for consistent method signatures in the SearchClient interface for different APIs.
            // (Throwing an XmlPullParserException would be fine, until dealing with an API using JSON, etc.)
            throw new IOException(e);
        }
        // Fix results
        for (Image image : imageList) {
            // Append values not returned by the API.
            image.webUrl = webUrlFromId(image.id);
            image.pixivId = Image.getPixivIdFromUrl(image.source);
            // Use original file if low-resolution sample does not exist.
            if (image.sampleUrl == null) {
                image.sampleUrl = image.fileUrl;
                image.sampleWidth = image.width;
                image.sampleHeight = image.height;
            }
        }
        // Create and return a SearchResult.
        return new SearchResult(imageList.toArray(new Image[0]), Tag.arrayFromString(tags), offset);
    }

    private boolean mutateImage(Image image, String name, String value) throws MalformedURLException {
        if (TextUtils.isEmpty(value))
            return false;
        boolean success = true;
        switch (name) {
            case "file_url" -> image.fileUrl = normalizeUrl(value);
            case "width" -> image.width = Integer.parseInt(value);
            case "height" -> image.height = Integer.parseInt(value);
            case "preview_url" -> image.previewUrl = normalizeUrl(value);
            case "preview_width" -> image.previewWidth = Integer.valueOf(value);
            case "preview_height" -> image.previewHeight = Integer.valueOf(value);
            case "sample_url" -> image.sampleUrl = normalizeUrl(value);
            case "sample_width" -> image.sampleWidth = Integer.valueOf(value);
            case "sample_height" -> image.sampleHeight = Integer.valueOf(value);
            case "tags" -> image.tags = Tag.arrayFromString(value, Tag.Type.GENERAL);
            case "id" -> image.id = value;
            case "parent_id" ->  image.parentId = value;
            case "rating" -> image.safeSearchRating = Image.SafeSearchRating.fromString(value);
            case "score" -> image.score = Integer.parseInt(value);
            case "md5" -> image.md5 = value;
            case "created_at", "date" -> {
                try {
                    image.createdAt = dateFromString(value);
                } catch (ParseException e) {
                    // There have been too many issues reported in Nori related to date parsing.
                    // It's almost as if every site uses its own date format and, unfortunately,
                    // I can't hard code all of them.
                    image.createdAt = null;
                }
            }
            default -> success = false;
        }
        return success;
    }

    /**
     * Convert a relative image URL to an absolute URL.
     *
     * @param url URL to convert.
     * @return Absolute URL.
     */
    protected String normalizeUrl(String url) throws java.net.MalformedURLException {
        // Return empty string for empty URLs.
        if (url == null || url.isEmpty()) {
            return "";
        }
        return new URL(new URL(apiEndpoint), url).toString();
    }

    /**
     * Get a URL viewable in the system web browser for given Image ID.
     *
     * @param id {@link Image} ID.
     * @return URL for viewing the image in the browser.
     */
    protected String webUrlFromId(String id) {
        return apiEndpoint + "/post/show/" + id;
    }

    private static final Predicate<String> numeric = Pattern.compile("^\\d+$").asMatchPredicate();
    /**
     * Create a {@link Date} object from String date representation used by this API.
     *
     * @param date Date string.
     * @return Date converted from given String.
     */
    protected Date dateFromString(String date) throws ParseException {
        if (numeric.test(date)) {
            // Moebooru-based boards (Danbooru 1.x fork) use Unix timestamps.
            return new Date(Integer.parseInt(date));
        } else {
            return DateUtils.parseDate(date, new String[] {
                    "yyyy-MM-dd HH:mm:ss", // Normal datetime
                    "EEE MMM dd HH:mm:ss Z yyyy" // Gelbooru
            });
        }
    }
    //endregion
}
