plugins {
    java
    application
    id("com.github.johnrengelman.shadow") version "7.0.0"
}

group = "io.gitlab.jfronny"
version = "2.0"

allprojects {
    group = rootProject.group

    repositories {
        mavenCentral()
        maven("https://m2.dv8tion.net/releases")
    }
}

application {
    mainClass.set("$group.${rootProject.name}.Main")
}

repositories {
    maven("https://jitpack.io") //kxml2 and lavaplayer
    // for xmlPull
    maven("https://gitlab.com/api/v4/projects/26729549/packages/maven") // repo for fetching `xmlpull` dependency that's java 9 enabled
}

dependencies {
    implementation("net.dv8tion:JDA:4.4.0_350") // https://github.com/DV8FromTheWorld/JDA#download
    //TODO switch back to upstream lavaplayer once that works again (or look for maintained forks, I guess)
    // https://github.com/sedmelluq/lavaplayer/pull/648
    //implementation("com.sedmelluq:lavaplayer:1.3.78") // https://github.com/sedmelluq/lavaplayer
    implementation(project(":lavaplayer:main")) //
    implementation("ch.qos.logback:logback-classic:1.2.10") // slf4j backend https://search.maven.org/artifact/ch.qos.logback/logback-classic
    implementation("org.ektorp:org.ektorp:1.5.0") // couchdb engine https://github.com/helun/Ektorp#ektorp--
    implementation("org.xmlpull:xmlpull:1.1.4.0")
    implementation("com.github.stefanhaustein:kxml2:2.5.0") // impl of xmlpull https://github.com/stefanhaustein/kxml2
    testImplementation("org.junit.jupiter:junit-jupiter:5.8.2")
}

tasks.test {
    useJUnitPlatform()
    testLogging {
        events("passed", "skipped", "failed")
    }
}
